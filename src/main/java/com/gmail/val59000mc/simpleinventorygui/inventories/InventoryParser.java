package com.gmail.val59000mc.simpleinventorygui.inventories;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.exceptions.InventoryParseException;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ItemParseException;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.items.ItemParser;

public class InventoryParser {

	public static Inventory parseInventorySection(ConfigurationSection inventorySection) throws InventoryParseException {

		String invTitle = inventorySection.getString("title");
		Integer invRows = inventorySection.getInt("rows", 6);
		Inventory inventory = new Inventory(inventorySection.getName(), invTitle, invRows);
		
		ConfigurationSection invItemsSection = inventorySection.getConfigurationSection("items");
		if(invItemsSection == null){
			throw new InventoryParseException("#Inventory '"+inventorySection.getName()+"' must provided an 'items' section");
		}
		
		for(String itemName : invItemsSection.getKeys(false)){
			
			Item item;
			try {
				item = ItemParser.parseItemSection(invItemsSection.getConfigurationSection(itemName));
			} catch (ItemParseException e) {
				throw new InventoryParseException("#When parsing inventory '"+inventorySection.getName()+"' at item '"+itemName+"' "+e.getMessage());
			}
			inventory.addItem(item);
		}
		
		return inventory;
	}

}
