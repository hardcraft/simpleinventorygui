package com.gmail.val59000mc.simpleinventorygui.inventories;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.items.ItemIndexer;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class InventoryManager {
	private static InventoryManager instance;
	
	private Set<Inventory> inventories;
	
	public static void reset(){
		if(instance != null){
			for(Inventory inv : instance.inventories){
				inv.removeItems();
			}
		}
		instance = new InventoryManager();
		instance.inventories = Collections.synchronizedSet(new HashSet<Inventory>());
	}
	
	public static InventoryManager instance(){
		return instance;
	}
	
	public Set<Inventory> getInventories(){
		return inventories;
	}
	
	public void addInventory(Inventory inventory){
		inventories.add(inventory);
	}
	
	public void removeInventory(Inventory inventory){
		inventory.removeItems();
		inventories.remove(inventory);
	}
	
	public Inventory getInventoryByName(String name){
		for(Inventory inv: getInventories()){
			if(inv.getName().equals(name)){
				return inv;
			}
		}
		return null;
	}

	public void updateInventoryView(Player player,SigPlayer sigPlayer) {
		InventoryView doubleInventory = player.getOpenInventory();
		updateInventoryView(player,sigPlayer,doubleInventory.getTopInventory());
		updateInventoryView(player,sigPlayer,doubleInventory.getBottomInventory());
	}
	
	public void updateInventoryView(Player player, SigPlayer sigPlayer, org.bukkit.inventory.Inventory inventory){
		ItemStack[] contents = inventory.getContents();
		for(int i = 0 ; i < contents.length ; i++){
			Item newItem = ItemIndexer.findItem(contents[i]);			
			if(newItem != null){
				contents[i] = newItem.buildItem(player,sigPlayer);
			}
		}
		inventory.setContents(contents);
	}
}
