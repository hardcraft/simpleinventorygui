package com.gmail.val59000mc.simpleinventorygui.inventories;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import com.gmail.val59000mc.simpleinventorygui.configuration.ConfigurationParser;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.items.ItemIndexer;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class Inventory {
	private Set<Item> items;
	private String name;
	private String title;
	private int rows;	
	
	// Constructor 
	public Inventory(String name, String title, int rows){
		this.name = name;
		this.rows = rows;
		this.items = Collections.synchronizedSet(new HashSet<Item>());
		this.title = ChatColor.translateAlternateColorCodes('&', title);
	}
	
	public void addItem(Item item){
		items.add(item);
		ItemIndexer.indexItem(item);
	}
	
	private org.bukkit.inventory.Inventory buildInventory(Player player,SigPlayer sigPlayer){
		org.bukkit.inventory.Inventory inv = Bukkit.createInventory(null, 9*rows, title);
		inv.setContents(this.getContents(player,sigPlayer));
		return inv;
	}
	
	private ItemStack[] getContents(Player player, SigPlayer sigPlayer){
		int maxSize = rows*9;
		ItemStack[] contents = new ItemStack[maxSize];
		for(Item item : items){
			if(item.getPosition() < maxSize){
				contents[item.getPosition()] = item.buildItem(player,sigPlayer);
			}
		}
		return contents;
	}
	
	
	// Accessors
	
	public Set<Item> getItems() {
		return items;
	}
	public String getName() {
		return name;
	}
	public String getTitle() {
		return title;
	}

	public void giveToPlayer(Player player, SigPlayer sigPlayer, boolean overrideInventory) {
		if(player != null){
			if(overrideInventory){
				ItemStack[] contents = getContents(player,sigPlayer);
				if(ConfigurationParser.setEmptySlotsWhenOverridingInventory){
					player.getInventory().setContents(contents);
				}else{
					PlayerInventory playerInventory = player.getInventory();
					for(int i = 0 ; i < contents.length ; i++){
						if(contents[i] != null && !contents[i].getType().equals(Material.AIR))
							playerInventory.setItem(i, contents[i]);
						if(i+1 == playerInventory.getSize())
							break;
					}
				}
			}else{
				PlayerInventory inv = player.getInventory();
				for(ItemStack stack : getContents(player,sigPlayer)){
					if(stack != null){
						inv.addItem(stack);
					}
				}
			}
		}
	}

	public void openInventory(Player player, SigPlayer sigPlayer) {
		player.openInventory(this.buildInventory(player,sigPlayer));
	}

	public void removeItems() {
		for(Item item : items){
			ItemIndexer.removeItemFromIndex(item);
		}
	}
	
	
}
