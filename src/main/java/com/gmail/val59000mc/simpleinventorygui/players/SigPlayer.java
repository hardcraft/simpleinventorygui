package com.gmail.val59000mc.simpleinventorygui.players;

import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.actions.toggle.Toggle;
import com.gmail.val59000mc.simpleinventorygui.actions.toggle.ToggleType;

public class SigPlayer {
	UUID uuid;
	Map<ToggleType,Toggle> toggles;
	
	public SigPlayer(Builder builder){
		this.uuid = builder.uuid;
		this.toggles = builder.toggles;
	}
	
	public static class Builder{
		private UUID uuid;
		private Map<ToggleType,Toggle> toggles;
		
		public Builder(UUID uuid, Map<ToggleType,Toggle> toggles){
			this.uuid = uuid;
			this.toggles = toggles;
		}
		
		public SigPlayer build(){
			return new SigPlayer(this);
		}
	}
	
	public Player getPlayer(){
		return Bukkit.getPlayer(uuid);
	}
	
	public boolean isOnline(){
		return Bukkit.getPlayer(uuid) != null;
	}
	
	
	// Accessors 

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public Map<ToggleType, Toggle> getToggles() {
		return toggles;
	}

	public void setToggles(Map<ToggleType, Toggle> toggles) {
		this.toggles = toggles;
	}
}
