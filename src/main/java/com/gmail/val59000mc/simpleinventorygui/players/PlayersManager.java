package com.gmail.val59000mc.simpleinventorygui.players;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.actions.toggle.Toggle;
import com.gmail.val59000mc.simpleinventorygui.actions.toggle.ToggleType;
import com.gmail.val59000mc.simpleinventorygui.persistence.IDatabase;

public class PlayersManager {
	private static IDatabase database;
	
	public static void setDatabase(IDatabase databaseImpl){
		database = databaseImpl;
	}
	
	public static SigPlayer getSigPlayer(Player player){
		return getSigPlayer(player.getUniqueId());
	}
	
	public static SigPlayer getSigPlayer(UUID uuid) {
		if(database == null)
			return new SigPlayer.Builder(uuid, new HashMap<ToggleType,Toggle>()).build();
		SigPlayer sigPlayer = database.getSigPlayer(uuid);
		return sigPlayer;
	}

	public static void saveSigPlayer(final SigPlayer sigPlayer) {
		if(database != null){
			Bukkit.getScheduler().runTaskAsynchronously(SIG.getPlugin(), new Runnable(){

				@Override
				public void run() {
					database.saveSigPlayer(sigPlayer);
				}
				
			});
		}
	}

	public static List<SigPlayer> getSigPlayers(List<UUID> uuidList) {
		return database.getSigPlayers(uuidList);
	}
	
	public static void saveSigPlayers(final List<SigPlayer> sigPlayers) {
		if(database != null){
			Bukkit.getScheduler().runTaskAsynchronously(SIG.getPlugin(), new Runnable(){

				@Override
				public void run() {
					database.saveSigPlayers(sigPlayers);
				}
				
			});
		}
	}
}
