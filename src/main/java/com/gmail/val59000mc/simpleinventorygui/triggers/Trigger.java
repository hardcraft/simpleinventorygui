package com.gmail.val59000mc.simpleinventorygui.triggers;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.ActionExecutor;
import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.configuration.Lang;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryManager;
import com.gmail.val59000mc.simpleinventorygui.items.ItemIndexer;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.spigotutils.Time;

public abstract class Trigger implements ActionExecutor{
	
	private List<String> permissions;
	private List<Action> actions;
	private int cooldown;
	private Map<UUID,Long> playersNextUses;
	private String groupCooldown;
	
	
	public Trigger(List<String> permissions, int cooldown, String groupCooldown, List<Action> actions){
		this.permissions = permissions;
		this.cooldown = cooldown;
		this.groupCooldown = groupCooldown;
		this.actions = actions;
		this.playersNextUses = new HashMap<UUID,Long>();
		
		for(Action action : actions){
			action.setActionExecutor(this);
		}
	}

	
	public void setGroupCooldown(String groupCooldown){
		this.groupCooldown = groupCooldown;
	}
	
	public abstract boolean isRelevant(TriggerTarget triggerEvent);
	
	private boolean checkCooldown(Player player){
		if(groupCooldown != null){
			return ItemIndexer.checkGroupCooldown(groupCooldown, player);
		}else if(cooldown > 0){
			UUID uuid = player.getUniqueId();
			Long nextUse = playersNextUses.get(uuid);
			Long now = Calendar.getInstance().getTimeInMillis();
			if(nextUse != null && nextUse > now){
				Long remainingTime = (nextUse-now)/1000;
				player.sendMessage(Lang.COOLDOWN_WAIT.replace("%time%", Time.getFormattedTime(remainingTime)));
				return false;
			}
		}
		return true;
	}
	
	private void updateCooldown(UUID uuid){
		if(groupCooldown != null){
			ItemIndexer.updateGroupCooldown(groupCooldown,uuid);
		}else if(cooldown > 0){
			Long nextUse = playersNextUses.get(uuid);
			Long now = Calendar.getInstance().getTimeInMillis();
			if(nextUse == null || nextUse <= now){
				playersNextUses.put(uuid, now+(cooldown*1000));
			}
		}
	}
	
	private boolean checkPermission(Player player){
		for(String node : permissions){
			if(!player.hasPermission(node)){
				player.sendMessage(Lang.NO_ITEM_PERMISSION);
				return false;
			}
		}
		return true;
	}
	
	// ActionExecutor

	@Override
	public void executeAllActionsAsynchronously(UUID playerUuid, SigPlayer sigPlayer){
		
		if(!ItemIndexer.isLocked(playerUuid) && !getActions().isEmpty()){
			Player player = Bukkit.getPlayer(playerUuid);
			if(player != null && player.isOnline()){
				if(checkPermission(player)){
					if(checkCooldown(player)){
						ItemIndexer.lock(playerUuid);
						getActions().get(0).executeAction(player, sigPlayer);
					}
				}	
				
			}
		}
		
	}
	
	public synchronized List<Action> getActions(){
		return actions;
	}

	@Override
	public void endActionsExecution(Player player, SigPlayer sigPlayer, boolean success) {
		UUID uuid = player.getUniqueId();
		ItemIndexer.unlock(uuid);
		if(success){
			updateCooldown(uuid);
		}
		if(player.isOnline()){
			InventoryManager.instance().updateInventoryView(player,sigPlayer);
			
		}
		
	}
	
	
	
}
