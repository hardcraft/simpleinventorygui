package com.gmail.val59000mc.simpleinventorygui.triggers;

import java.util.UUID;

import org.bukkit.block.Sign;
import org.bukkit.entity.Entity;

public class TriggerTarget {

	private Sign sign;
	private Entity entity;
	private UUID entityUUID;

	public TriggerTarget(Entity entity) {
		super();
		this.entity = entity;
		this.entityUUID = entity.getUniqueId();
	}
	
	public TriggerTarget(Sign sign) {
		super();
		this.sign = sign;
	}
	
	public Entity getEntity() {
		return entity;
	}

	public UUID getEntityUUID() {
		return entityUUID;
	}

	public Sign getSign() {
		return sign;
	}	
	
	
	
}
