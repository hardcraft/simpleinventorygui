package com.gmail.val59000mc.simpleinventorygui.triggers;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryManager;
import com.gmail.val59000mc.simpleinventorygui.items.ItemIndexer;
import com.gmail.val59000mc.simpleinventorygui.players.PlayersManager;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class TriggerManager {
	private static TriggerManager instance;

	private static Set<Trigger> triggers;
	
	public static void reset(){
		instance = new TriggerManager();
		triggers = Collections.synchronizedSet(new HashSet<Trigger>());
	}
	
	public static synchronized Set<Trigger> getTriggers(){
		return triggers;
	}
	
	public static TriggerManager instance(){
		return instance;
	}
	
	public void addTrigger(Trigger trigger){
		triggers.add(trigger);
	}
	
	public boolean executeTriggers(UUID playerUuid, TriggerTarget triggerTarget){
		 
		if(ItemIndexer.isLocked(playerUuid)){
			return false;
		}
		
	    Trigger relevantTrigger = getRelevantTrigger(triggerTarget);
		
		if(relevantTrigger != null){
			Bukkit.getScheduler().runTaskAsynchronously(SIG.getPlugin(), new Runnable(){

				@Override
				public void run() {
					final SigPlayer sigPlayer = PlayersManager.getSigPlayer(playerUuid);
					
					Bukkit.getScheduler().runTask(SIG.getPlugin(), new Runnable(){

						@Override
						public void run() {
							relevantTrigger.executeAllActionsAsynchronously(playerUuid, sigPlayer);
						}
						
					});
				}
				
			});
			
			return true;
		}
		
		return false;
	}

	private Trigger getRelevantTrigger(TriggerTarget triggerTarget) {
		for(Trigger trigger : getTriggers()){
			if(trigger.isRelevant(triggerTarget)){
				return trigger;
			}
		}
		return null;
	}
}
