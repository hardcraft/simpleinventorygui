package com.gmail.val59000mc.simpleinventorygui.triggers;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;

public class RightClickSignTrigger extends Trigger {
	private BlockState blockState;
	private Double x,y,z;
	private Float yaw, pitch;
	private String world;
	private Location location;
	private boolean invalidLocation;
	
	public RightClickSignTrigger(List<String> permissions, int cooldown, String groupCooldown, List<Action> actions, String world, Double x, Double y, Double z, Float yaw, Float pitch) {
		super(permissions,cooldown, groupCooldown, actions);
		this.blockState = null;
		this.world = world;
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = yaw;
		this.pitch = pitch;
		this.invalidLocation = false;
	}
	
	@Override
	public boolean isRelevant(TriggerTarget triggerEvent) {
		Sign sign = triggerEvent.getSign();
		
		if(sign == null || invalidLocation){
			return false;
		}
		
		// Set location if null
		if(location == null){
			this.location = new Location(Bukkit.getWorld(world),x,y,z,yaw,pitch);
			if(location != null && location.getWorld() != null){
				this.location = new Location(location.getWorld(), location.getBlockX(), location.getBlockY(), location.getBlockZ());
			}
		}
		
		if(location == null || location.getWorld() == null){
			this.invalidLocation = true;
			return false;
		}
		
		if(blockLocationEquals(sign.getLocation(), location) && location.getWorld().equals(sign.getLocation().getWorld())){
			return true;
		}
		return false;
		
	}
	
	public boolean blockLocationEquals(Location a, Location b){
		return a.getBlockX() == b.getBlockX()
				&& a.getBlockY() == b.getBlockY()
				&& a.getBlockZ() == b.getBlockZ();
	}
	
	
}
