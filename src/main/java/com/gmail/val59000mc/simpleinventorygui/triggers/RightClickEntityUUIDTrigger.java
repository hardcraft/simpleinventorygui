package com.gmail.val59000mc.simpleinventorygui.triggers;

import java.util.List;
import java.util.UUID;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.spigotutils.Logger;

public class RightClickEntityUUIDTrigger extends Trigger {
	private UUID uuid;
	
	public RightClickEntityUUIDTrigger(List<String> permissions, int cooldown, String groupCooldown, List<Action> actions, UUID uuid) {
		super(permissions,cooldown,groupCooldown, actions);
		this.uuid = uuid;
	}
	
	@Override
	public boolean isRelevant(TriggerTarget triggerEvent) {
		UUID entityUUID = triggerEvent.getEntityUUID();
		if(entityUUID == null){
			return false;
		}
		Logger.debug("config uuid = "+uuid.toString());
		Logger.debug("entity uuid = "+entityUUID.toString());
		return uuid.equals(entityUUID);
	}
	
	
}
