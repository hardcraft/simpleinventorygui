package com.gmail.val59000mc.simpleinventorygui.triggers;

public enum TriggerType {
	RIGHT_CLICK_ENTITY,
	RIGHT_CLICK_ENTITY_UUID,
	RIGHT_CLICK_SIGN;
}
