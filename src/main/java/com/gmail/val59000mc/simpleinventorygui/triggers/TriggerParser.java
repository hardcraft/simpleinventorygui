package com.gmail.val59000mc.simpleinventorygui.triggers;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.parsers.ActionParserManager;
import com.gmail.val59000mc.simpleinventorygui.configuration.ConfigurationParser;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;
import com.gmail.val59000mc.simpleinventorygui.exceptions.TriggerParseException;

public class TriggerParser {

	public static Trigger parseTriggerSection(ConfigurationSection triggerSection) throws TriggerParseException{
		Trigger trigger = null;

		/////////////////////
		// Cooldown String //
		String cooldownName = triggerSection.getString("cooldown");
		Integer cooldown = triggerSection.getInt("cooldown",ConfigurationParser.defaultItemCooldown);
		if(cooldown < 0 || cooldown > 10000)
			throw new TriggerParseException("#Trigger cooldown must be between 0 and 10000");
		
		/////////////////
		// Permissions //
		List<String> permissions = triggerSection.getStringList("permissions");
		if(permissions == null){
			permissions = new ArrayList<String>();
		}
		
		////////////////////
		// Action section //
		ConfigurationSection actionSection = triggerSection.getConfigurationSection("actions");
		List<Action> actions;
		try {
			actions = ActionParserManager.instance().parseAllActionsSection(actionSection);
		} catch (ActionParseException e) {
			throw new TriggerParseException("#When parsing actions : "+e.getMessage());
		}
		
		String triggerTypeStr = triggerSection.getString("type");
		TriggerType triggerType;
		if(triggerTypeStr == null)
			throw new TriggerParseException("#The trigger '"+triggerSection.getName()+"' must have a valid type");
		triggerTypeStr = triggerTypeStr.replaceAll("-", "_");
		try {
			triggerType = TriggerType.valueOf(triggerTypeStr.toUpperCase());
		} catch (IllegalArgumentException e) {
			throw new TriggerParseException("#The trigger '"+triggerSection.getName()+"' must have a valid type");
		}
		
		switch(triggerType){
			case RIGHT_CLICK_ENTITY_UUID:
				String uuidStr = triggerSection.getString("uuid");
				if(uuidStr == null){
					throw new TriggerParseException("#The right-click-entity-uuid trigger '"+triggerSection.getName()+"' must provide a 'uuid' attribute, example '5c7337d5-903e-4e13-9061-06b4384074ea'");		
				}
				
				UUID entityUUID;
				try {
					entityUUID = UUID.fromString(uuidStr);
				} catch (NumberFormatException e) {
					throw new TriggerParseException("#The right-click-entity-uuid action '"+triggerSection.getName()+"' must provide a valid 'uuid' attribute, example '5c7337d5-903e-4e13-9061-06b4384074ea'");		
				}
				
				trigger = new RightClickEntityUUIDTrigger(permissions, cooldown, cooldownName, actions, entityUUID);
				
			break;
			
			case RIGHT_CLICK_ENTITY:
				
				// location
				
				String entitylocStr = triggerSection.getString("location");
				if(entitylocStr == null){
					throw new TriggerParseException("#The right-click-entity trigger '"+triggerSection.getName()+"' must provide a 'location' attribute, example 'world:0:60:0:180:0'");		
				}
				String[] locArr = entitylocStr.split(":");
				if(locArr.length != 6){
					throw new TriggerParseException("#The right-click-entity trigger '"+triggerSection.getName()+"' must have a valid 'location' attribute , example 'world:0:60:0:180:0'");		
				}
				
				Double x,y,z;
				Float yaw,pitch;
				String world;
				try {
					x = Double.parseDouble(locArr[1]);
					y = Double.parseDouble(locArr[2]);
					z = Double.parseDouble(locArr[3]);
					yaw = Float.parseFloat(locArr[4]);
					pitch = Float.parseFloat(locArr[5]);
				} catch (NumberFormatException e) {
					throw new TriggerParseException("#The right-click-entity trigger '"+triggerSection.getName()+"' must provide a valid 'location' attribute', example 'world:0:60:0:180:0'");		
				}
				world = locArr[0];
				
				
				// entity type
				String entityTypeStr = triggerSection.getString("entity-type");
				EntityType type;
				if(entityTypeStr == null){
					throw new TriggerParseException("#The right-click-entity trigger '"+triggerSection.getName()+"' must provide a 'entity-type' attribute, example 'villager'");		
				}
				try{
					type = EntityType.valueOf(entityTypeStr.toUpperCase());
				}catch(IllegalArgumentException e){
					throw new TriggerParseException("#The right-click-entity trigger '"+triggerSection.getName()+"' must provide a valid 'entity-type' attribute, example 'villager'");		
				}
				
				trigger = new RightClickEntityTrigger(permissions, cooldown, cooldownName, actions, type, world, x, y, z, yaw, pitch);
				break;
			case RIGHT_CLICK_SIGN:
				
				// location
				
				String signLocStr = triggerSection.getString("location");
				if(signLocStr == null){
					throw new TriggerParseException("#The right-click-sign trigger '"+triggerSection.getName()+"' must provide a 'location' attribute, example 'world:0:60:0:180:0'");		
				}
				String[] signLocArr = signLocStr.split(":");
				if(signLocArr.length != 6){
					throw new TriggerParseException("#The right-click-sign trigger '"+triggerSection.getName()+"' must have a valid 'location' attribute , example 'world:0:60:0:180:0'");		
				}
				
				Double signX,signY,signZ;
				Float signYaw,signPitch;
				String signWorld;
				try {
					signX = Double.parseDouble(signLocArr[1]);
					signY = Double.parseDouble(signLocArr[2]);
					signZ = Double.parseDouble(signLocArr[3]);
					signYaw = Float.parseFloat(signLocArr[4]);
					signPitch = Float.parseFloat(signLocArr[5]);
				} catch (NumberFormatException e) {
					throw new TriggerParseException("#The right-click-sign trigger '"+triggerSection.getName()+"' must provide a valid 'location' attribute', example 'world:0:60:0:180:0'");		
				}
				signWorld = signLocArr[0];
				
				trigger = new RightClickSignTrigger(permissions, cooldown, cooldownName, actions, signWorld, signX, signY, signZ, signYaw, signPitch);
				break;
			default:
				throw new TriggerParseException("#The trigger '"+triggerSection.getName()+"' must have a valid type");	
		}

		return trigger;
	}
		
}
