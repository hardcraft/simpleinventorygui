package com.gmail.val59000mc.simpleinventorygui.triggers;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;

public class RightClickEntityTrigger extends Trigger {
	private EntityType type;
	private Double x,y,z;
	private Float yaw, pitch;
	private String world;
	private Location location;
	private boolean invalidLocation;
	
	public RightClickEntityTrigger(List<String> permissions, int cooldown, String groupCooldown, List<Action> actions, EntityType type, String world, Double x, Double y, Double z, Float yaw, Float pitch) {
		super(permissions,cooldown, groupCooldown, actions);
		this.type = type;
		this.world = world;
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = yaw;
		this.pitch = pitch;
		this.invalidLocation = false;
	}
	
	@Override
	public boolean isRelevant(TriggerTarget triggerEvent) {
		Entity entity = triggerEvent.getEntity();
		
		if(entity == null || invalidLocation){
			return false;
		}
		
		// Set location if null
		if(location == null){
			this.location = new Location(Bukkit.getWorld(world),x,y,z,yaw,pitch);
		}
		
		if(location == null || location.getWorld() == null){
			this.invalidLocation = true;
			return false;
		}
		
		if(entity.getType().equals(type) && location.getWorld().equals(entity.getLocation().getWorld())){
			Double squaredDistance = entity.getLocation().distanceSquared(location);
			if(squaredDistance < 2){
				return true;
			}
		}
		return false;
		
	}
	
	
}
