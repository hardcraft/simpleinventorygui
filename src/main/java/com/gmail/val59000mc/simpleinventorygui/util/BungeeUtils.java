package com.gmail.val59000mc.simpleinventorygui.util;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.ChannelNotRegisteredException;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.listeners.BungeeSubChannel;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

public class BungeeUtils {
	


	public static void sendPlayerToServer(UUID uuid, String serverName) {
		sendPlayerToServer(Bukkit.getPlayer(uuid),serverName);
	}
	
	public static void sendPlayerToServer(Player player, String serverName){
		if(player != null){
			try{
				ByteArrayDataOutput out  = ByteStreams.newDataOutput();
		        out.writeUTF("Connect");
		        out.writeUTF(serverName);
				player.sendPluginMessage(SIG.getPlugin(), "BungeeCord" , out.toByteArray());
			}catch(ChannelNotRegisteredException e){
				String err = "Cannot send "+player.getName()+" to "+serverName+" :  you are not on a bungee server";
				player.sendMessage(ChatColor.RED+err);
				Bukkit.getLogger().warning(err);
			}
		}
	}
	
	public static void sendCustomMessage(String serverName, BungeeSubChannel subChannel, List<String> arguments) {
		
		ByteArrayDataOutput out  = ByteStreams.newDataOutput();
    	out.writeUTF("Forward"); // method
		out.writeUTF(serverName); // server name
		out.writeUTF(subChannel.toString()); // sub channel name
		
		ByteArrayDataOutput argumentsOutput = ByteStreams.newDataOutput();

		for(String arg : arguments){
			argumentsOutput.writeUTF(arg);
		}
		out.writeShort(argumentsOutput.toByteArray().length);
		out.write(argumentsOutput.toByteArray());
		
		Bukkit.getServer().sendPluginMessage(SIG.getPlugin(), "BungeeCord", out.toByteArray());
    }
	
	public static void sendMessage(String... args){
		final Iterator<? extends Player> players = Bukkit.getOnlinePlayers().iterator();
		if(players.hasNext()){
			BungeeUtils.sendMessage(players.next(), args);
		}
    }
	
	public static void sendMessage(Player player,String... args){
		ByteArrayDataOutput out  = ByteStreams.newDataOutput();
		for (final String arg : args) {
			out.writeUTF(arg);
        }
		
		player.sendPluginMessage(SIG.getPlugin(), "BungeeCord", out.toByteArray());
	}
}
