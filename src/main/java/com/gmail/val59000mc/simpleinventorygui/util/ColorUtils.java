package com.gmail.val59000mc.simpleinventorygui.util;

import org.bukkit.Color;

public class ColorUtils {
	
	public static Color fromHexa(String hexa){
		hexa = hexa.replace("#", "");
		if(hexa.length() != 6){
			throw new IllegalArgumentException("Hexadeciam color length must be 6 characters");
		}
		

		return Color.fromRGB(Integer.valueOf( hexa.substring( 0, 2 ), 16 ),
				Integer.valueOf( hexa.substring( 2, 4 ), 16 ),
				Integer.valueOf( hexa.substring( 4, 6 ), 16 ));		
	}
	
}
