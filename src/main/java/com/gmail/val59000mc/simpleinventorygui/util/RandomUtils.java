package com.gmail.val59000mc.simpleinventorygui.util;

import org.apache.commons.lang.RandomStringUtils;


public class RandomUtils {
	
	public static String getRandomHexadecimalString(int length){
		return RandomStringUtils.randomAlphanumeric(length).toUpperCase();
	}
}
