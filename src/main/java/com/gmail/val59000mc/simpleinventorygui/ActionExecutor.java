package com.gmail.val59000mc.simpleinventorygui;

import java.util.List;
import java.util.UUID;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public interface ActionExecutor {

	public List<Action> getActions();

	public void executeAllActionsAsynchronously(UUID playerUuid, SigPlayer sigPlayer);
	
	public void endActionsExecution(Player player, SigPlayer sigPlayer, boolean success);
	
	default public Action getNextAction(Action action){
		if(action == null){
			return null;
		}
		int index = getActions().indexOf(action);
		if(index == -1 || index == getActions().size()-1){
			return null;
		}
		return getActions().get(index+1);
	}
}
