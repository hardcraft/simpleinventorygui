package com.gmail.val59000mc.simpleinventorygui.exceptions;

public class BountifulApiLoadingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2970834186240056524L;

	public BountifulApiLoadingException(String string) {
		super(string);
	}

}
