package com.gmail.val59000mc.simpleinventorygui.exceptions;

public class DatabaseConnectionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2159189920581043298L;

	public DatabaseConnectionException(String message){
		super(message);
	}
}
