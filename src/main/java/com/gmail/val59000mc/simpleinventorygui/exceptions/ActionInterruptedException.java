package com.gmail.val59000mc.simpleinventorygui.exceptions;

public class ActionInterruptedException extends Exception{

		/**
	 * 
	 */
	private static final long serialVersionUID = 6941321751648336138L;

		public ActionInterruptedException (String message){
			super(message);
		}
}
