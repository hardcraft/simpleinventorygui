package com.gmail.val59000mc.simpleinventorygui.exceptions;

public class EffectLibLoadingException extends Exception{

	
		/**
	 * 
	 */
	private static final long serialVersionUID = 2500567300185785351L;

		public EffectLibLoadingException (String message){
			super(message);
		}
}
