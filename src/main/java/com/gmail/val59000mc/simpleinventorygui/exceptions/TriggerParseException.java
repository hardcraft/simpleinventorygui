package com.gmail.val59000mc.simpleinventorygui.exceptions;

public class TriggerParseException extends Exception{

		/**
	 * 
	 */
	private static final long serialVersionUID = -437129280349598165L;

		/**
	 * 
	 */

		public TriggerParseException (String message){
			super(message);
		}
}
