package com.gmail.val59000mc.simpleinventorygui.exceptions;

public class ActionParseException extends Exception{


		/**
	 * 
	 */
	private static final long serialVersionUID = 4112061049406405354L;

		public ActionParseException (String message){
			super(message);
		}
}
