package com.gmail.val59000mc.simpleinventorygui.exceptions;

public class ItemParseException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6826677564176851067L;
	
	public ItemParseException(String message){
		super(message);
	}
}
