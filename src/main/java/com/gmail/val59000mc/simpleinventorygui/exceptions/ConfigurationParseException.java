package com.gmail.val59000mc.simpleinventorygui.exceptions;

public class ConfigurationParseException extends Exception{

		/**
	 * 
	 */
	private static final long serialVersionUID = 6941321751648336138L;

		public ConfigurationParseException (String message){
			super(message);
		}
}
