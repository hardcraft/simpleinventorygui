package com.gmail.val59000mc.simpleinventorygui.exceptions;

public class WrongEffectParameterException extends Exception{

		/**
	 * 
	 */
	private static final long serialVersionUID = -1491630086808390846L;

		public WrongEffectParameterException (String message){
			super(message);
		}
}
