package com.gmail.val59000mc.simpleinventorygui.exceptions;

public class VaultLoadingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2970834186240056524L;

	public VaultLoadingException(String string) {
		super(string);
	}

}
