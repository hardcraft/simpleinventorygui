package com.gmail.val59000mc.simpleinventorygui.exceptions;

public class SQLibraryLoadingException extends Exception {

	public SQLibraryLoadingException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 3388057960979526569L;

}
