package com.gmail.val59000mc.simpleinventorygui.api;

import java.io.File;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.actions.parsers.ActionParser;
import com.gmail.val59000mc.simpleinventorygui.actions.parsers.ActionParserManager;
import com.gmail.val59000mc.simpleinventorygui.configuration.ConfigurationParser;
import com.gmail.val59000mc.simpleinventorygui.exceptions.InventoryParseException;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ItemParseException;
import com.gmail.val59000mc.simpleinventorygui.exceptions.TriggerParseException;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryManager;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryParser;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.items.ItemParser;
import com.gmail.val59000mc.simpleinventorygui.placeholders.IPlaceholder;
import com.gmail.val59000mc.simpleinventorygui.placeholders.PlaceholderManager;
import com.gmail.val59000mc.simpleinventorygui.placeholders.SimplePlaceholder;
import com.gmail.val59000mc.simpleinventorygui.players.PlayersManager;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.simpleinventorygui.triggers.Trigger;
import com.gmail.val59000mc.simpleinventorygui.triggers.TriggerParser;
import com.google.common.base.Preconditions;

public class SIGApi {
	
	public void giveInventoryToPlayer(Player player, String inventoryName){
		UUID uuid = player.getUniqueId();
		
		Bukkit.getScheduler().runTaskAsynchronously(SIG.getPlugin(), new Runnable() {

			@Override
			public void run() {

				final SigPlayer sigPlayer = PlayersManager.getSigPlayer(uuid);

				Bukkit.getScheduler().runTask(SIG.getPlugin(), new Runnable(){

					@Override
					public void run() {

						if(sigPlayer.isOnline()){
							Inventory inv = InventoryManager.instance().getInventoryByName(inventoryName);
							Preconditions.checkNotNull(inv, "No inventory called '"+inventoryName+"' has been registered.");
							inv.giveToPlayer(sigPlayer.getPlayer(),sigPlayer,ConfigurationParser.giveInventoryOnJoinOverridesPlayerInventory);
						}	
						
					}
					
					
				});
				
				
			}
			
		});					
	}
	
	public void openInventoryForPlayer(Player player, String inventoryName){
		UUID uuid = player.getUniqueId();
		
		Bukkit.getScheduler().runTaskAsynchronously(SIG.getPlugin(), new Runnable() {

			@Override
			public void run() {

				final SigPlayer sigPlayer = PlayersManager.getSigPlayer(uuid);

				Bukkit.getScheduler().runTask(SIG.getPlugin(), new Runnable(){

					@Override
					public void run() {

						if(sigPlayer.isOnline()){
							Inventory inv = InventoryManager.instance().getInventoryByName(inventoryName);
							Preconditions.checkNotNull(inv, "No inventory called '+invName+' has been registered.");
							inv.openInventory(player, sigPlayer);
						}	
						
					}
					
					
				});
				
				
			}
			
		});					
	}

	@Deprecated
	public Inventory registerInventory(Inventory inventory){
		InventoryManager.instance().addInventory(inventory);
		return inventory;
	}

	@Deprecated
	public Inventory registerInventory(String name, String title, int rows){
		Inventory inventory = new Inventory(name, title, rows);
		InventoryManager.instance().addInventory(inventory);
		return inventory;
	}

	@Deprecated
	public Inventory registerItem(Inventory inventory, Item item){
		inventory.addItem(item);
		return inventory;
	}

	@Deprecated
	public Inventory registerItem(String invName, Item item){
		Inventory inventory = InventoryManager.instance().getInventoryByName(invName);
		Preconditions.checkNotNull(inventory, "No inventory called '+invName+' has been registered.");
		inventory.addItem(item);
		return inventory;
	}

	/**
	 * @deprecated Listen to SIFRegisterPlaceholderEvent to register placeholder when plugin loads or reloads
	 * @param placeholder
	 */
	@Deprecated
	public void registerPlaceholder(IPlaceholder placeholder){
		PlaceholderManager.instance().registerNewPlaceholder(placeholder);
	}

	/**
	 * @deprecated Listen to SIFRegisterPlaceholderEvent to register placeholder when plugin loads or reloads
	 * @param original
	 * @param replaceWith
	 */
	@Deprecated
	public void registerTextPlaceholder(String original, String replaceWith){
		PlaceholderManager.instance().registerNewPlaceholder(new SimplePlaceholder(original) {
			
			@Override
			public String replacePlaceholder(String original, Player player, SigPlayer sigPlayer) {
				return original.replaceAll(getPattern(), replaceWith);
			}
			
		});
	}

	@Deprecated
	public void registerActionParser(ActionParser actionParser){
		ActionParserManager.instance().registerActionParser(actionParser);
	}
	
	public Item parseItemSection(ConfigurationSection section) throws ItemParseException{
		return ItemParser.parseItemSection(section);
	}
	
	public Item.ItemBuilder parseItemString(String string) throws ItemParseException{
		return ItemParser.parseItemString(string);
	}
	
	public Trigger parseTriggerSection(ConfigurationSection section) throws TriggerParseException{
		return TriggerParser.parseTriggerSection(section);
	}
	
	public Inventory parseInventorySection(ConfigurationSection section) throws InventoryParseException{
		return InventoryParser.parseInventorySection(section);
	}
	
	public void registerConfigurationFile(File file) throws InventoryParseException, TriggerParseException{
		ConfigurationParser.registerConfigurationFile(file);
	}
}
