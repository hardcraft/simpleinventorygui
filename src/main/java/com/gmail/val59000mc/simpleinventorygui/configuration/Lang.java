package com.gmail.val59000mc.simpleinventorygui.configuration;

import java.io.File;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.gmail.val59000mc.simpleinventorygui.SIG;

public class Lang {
	
	public static String COOLDOWN_WAIT;
	public static String NO_ITEM_PERMISSION;
	public static String ALREADY_BOUGHT_PERMISSION;
	public static String COULD_NOT_ACCESS_PERMISSION_PLUGIN;
	public static String COULD_NOT_REMOVE_PERMISSION;
	public static String NOT_ENOUGH_MONEY;
	public static String NOT_ENOUGH_ITEMS;
	public static String ITEM_BOUGHT;
	public static String ITEM_SOLD;
	public static String COULD_NOT_PLAY_EFFECT;
	public static String TOGGLE;
	public static String COULD_NOT_FIND_WORLD;
	
	
	public static void loadLang(){
		saveDefaultLang();
		File file = new File(SIG.getPlugin().getDataFolder(),"lang.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		COOLDOWN_WAIT = getLang(cfg,"cooldown-wait");
		NO_ITEM_PERMISSION = getLang(cfg,"no-item-permission");
		ALREADY_BOUGHT_PERMISSION = getLang(cfg,"already-bought-permission");
		COULD_NOT_ACCESS_PERMISSION_PLUGIN = getLang(cfg,"could-not-access-permission-plugin");
		COULD_NOT_REMOVE_PERMISSION = getLang(cfg,"could-not-remove-permission");
		NOT_ENOUGH_MONEY = getLang(cfg, "not-enough-money");
		NOT_ENOUGH_ITEMS = getLang(cfg, "not-enough-items");
		ITEM_BOUGHT = getLang(cfg, "item-bought");
		ITEM_SOLD = getLang(cfg, "item-sold");
		COULD_NOT_PLAY_EFFECT = getLang(cfg, "could-not-play-effect");
		TOGGLE = getLang(cfg, "toggle");
		COULD_NOT_FIND_WORLD = getLang(cfg, "could-not-find-world");
		 
		
	}
	
	private static String getLang(FileConfiguration cfg, String path){
		return ChatColor.translateAlternateColorCodes('&', cfg.getString(path,"UNKNOWN"));
	}
	
	private static void saveDefaultLang(){
		
		SIG plugin = SIG.getPlugin();
		
		if(!plugin.getDataFolder().exists()){
			plugin.getDataFolder().mkdirs();
		}
		
		if(!new File(plugin.getDataFolder(),"lang.yml").exists()){
			plugin.copy(plugin.getResource("lang.yml"), new File(plugin.getDataFolder(),"lang.yml"));
		}
		
	}
	
}
