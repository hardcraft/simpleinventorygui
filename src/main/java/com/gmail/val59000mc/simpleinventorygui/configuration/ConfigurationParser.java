package com.gmail.val59000mc.simpleinventorygui.configuration;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.actions.parsers.ActionParserManager;
import com.gmail.val59000mc.simpleinventorygui.events.*;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ConfigurationParseException;
import com.gmail.val59000mc.simpleinventorygui.exceptions.DatabaseConnectionException;
import com.gmail.val59000mc.simpleinventorygui.exceptions.InventoryParseException;
import com.gmail.val59000mc.simpleinventorygui.exceptions.TriggerParseException;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryManager;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryParser;
import com.gmail.val59000mc.simpleinventorygui.items.GroupCooldown;
import com.gmail.val59000mc.simpleinventorygui.items.ItemIndexer;
import com.gmail.val59000mc.simpleinventorygui.persistence.IDatabase;
import com.gmail.val59000mc.simpleinventorygui.placeholders.PlaceholderManager;
import com.gmail.val59000mc.simpleinventorygui.players.PlayersManager;
import com.gmail.val59000mc.simpleinventorygui.triggers.Trigger;
import com.gmail.val59000mc.simpleinventorygui.triggers.TriggerManager;
import com.gmail.val59000mc.simpleinventorygui.triggers.TriggerParser;
import com.gmail.val59000mc.spigotutils.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.FilenameFilter;
import java.util.regex.Pattern;

public class ConfigurationParser {

    // debug
    public static boolean debug;

    // general config options
    public static String giveInventoryOnJoin;
    public static boolean giveInventoryOnJoinOverridesPlayerInventory;
    public static boolean setEmptySlotsWhenOverridingInventory;
    public static boolean playerVisibilityToggle;
    public static String databaseClass;
    public static int defaultItemCooldown;
    public static boolean canThrowItems;

    // dependencies
    public static boolean effectLibLoaded;
    public static boolean vaultLoaded;
    public static boolean bountifulApiLoaded;

    public static boolean loadConfiguration() {
        InventoryManager.reset();
        ItemIndexer.reset();
        TriggerManager.reset();
        ActionParserManager.reset();
        PlaceholderManager.reset();

        Bukkit.getPluginManager().callEvent(new SIGRegisterPlaceholderEvent(PlaceholderManager.instance()));
        Bukkit.getPluginManager().callEvent(new SIGRegisterActionParserEvent(ActionParserManager.instance()));
        Bukkit.getPluginManager().callEvent(new SIGRegisterInventoryEvent(InventoryManager.instance()));
        Bukkit.getPluginManager().callEvent(new SIGRegisterTriggerEvent(TriggerManager.instance()));

        SIG.getPlugin().reloadConfig();
        FileConfiguration cfg = SIG.getPlugin().getConfig();

        // Loading configuration section
        try {
            loadConfigOptions(cfg.getConfigurationSection("configuration"));
            Bukkit.getLogger().info("[SimpleInventoryGUI] Successfully parsed 'configuration' section");
        } catch (ConfigurationParseException e) {
            sendColoredMessageToConsole(ChatColor.RED + "*********************************");
            Bukkit.getLogger().severe("[SimpleInventoryGUI] Error when parsing 'configuration' : ");
            for (String error : e.getMessage().split(Pattern.quote("#"))) {
                if (!error.isEmpty()) {
                    Bukkit.getLogger().severe(error);
                }
            }
            sendColoredMessageToConsole(ChatColor.RED + "*********************************");
            return false;
        }

        // Load all yml files except lang.yml and db.yml

        File dir = SIG.getPlugin().getDataFolder();
        File[] files = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                if (name.equals("db.yml") || name.equals("lang.yml") || name.equals("mysql_credentials.yml")) {
                    return false;
                }

                return name.toLowerCase().endsWith(".yml");
            }
        });

        for (File file : files) {

            try {
                registerConfigurationFile(file);
            } catch (InventoryParseException | TriggerParseException e) {
                return false;
            }

        }

        Bukkit.getPluginManager().callEvent(new SIGRegisterConfigurationFileEvent());


        // Loading database
        IDatabase database;
        if(databaseClass != null) {
            try {
                database = SIG.getPlugin().openDatabaseConnection(databaseClass);
                PlayersManager.setDatabase(database);
            } catch (DatabaseConnectionException e) {
                sendColoredMessageToConsole(ChatColor.RED + "*********************************");
                Bukkit.getLogger().severe("[SimpleInventoryGUI] " + e.getMessage());
                sendColoredMessageToConsole(ChatColor.RED + "*********************************");
                return false;
            }
        }

        sendColoredMessageToConsole(ChatColor.GREEN + "[SimpleInventoryGUI] Successfuly loaded !");
        return true;
    }

    public static boolean registerConfigurationFile(File file) throws InventoryParseException, TriggerParseException {

        FileConfiguration yml;
        try {
            yml = YamlConfiguration.loadConfiguration(file);
        } catch (Exception e) {
            Logger.severeC(ChatColor.RED + "*********************************");
            Logger.severeC(e.getMessage());
            e.printStackTrace();
            sendColoredMessageToConsole(ChatColor.RED + "*********************************");
            throw e;
        }

        Logger.info("Reading config file '" + file.getName() + "'");


        // Loading inventories
        try {
            loadInventories(yml.getConfigurationSection("inventories"));
        } catch (InventoryParseException e) {
            sendColoredMessageToConsole(ChatColor.RED + "*********************************");
            Bukkit.getLogger().severe("[SimpleInventoryGUI] Error when parsing 'inventories' in file '" + file.getName() + "' :");
            for (String error : e.getMessage().split(Pattern.quote("#"))) {
                if (!error.isEmpty()) {
                    Bukkit.getLogger().severe(error);
                }
            }
            sendColoredMessageToConsole(ChatColor.RED + "*********************************");
            throw e;
        }

        // Loading triggers
        try {
            loadTriggers(yml.getConfigurationSection("triggers"));
        } catch (TriggerParseException e) {
            sendColoredMessageToConsole(ChatColor.RED + "*********************************");
            Bukkit.getLogger().severe("[SimpleInventoryGUI] Error when parsing 'triggers' in file '" + file.getName() + "' :");
            for (String error : e.getMessage().split(Pattern.quote("#"))) {
                if (!error.isEmpty()) {
                    Bukkit.getLogger().severe(error);
                }
            }
            sendColoredMessageToConsole(ChatColor.RED + "*********************************");
            throw e;
        }

        return true;

    }

    private static void sendColoredMessageToConsole(String message) {
        ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
        console.sendMessage(message);
    }

    private static void loadConfigOptions(ConfigurationSection configuration) throws ConfigurationParseException {
        if (configuration == null)
            throw new ConfigurationParseException("#You must provide a configuration section");

        // debug
        debug = configuration.getBoolean("debug", false);
        Logger.setDebug(debug);

        // give-inventory-on-join
        giveInventoryOnJoin = configuration.getString("give-inventory-on-join", "false");

        // default-item-cooldown
        defaultItemCooldown = configuration.getInt("default-item-cooldown", 0);
        if (defaultItemCooldown < 0 || defaultItemCooldown > 1000) {
            throw new ConfigurationParseException("#'default-item-cooldown' must be between 0 and 1000");
        }

        // can-throw-items
        canThrowItems = configuration.getBoolean("can-throw-items", false);

        // group-cooldowns
        ConfigurationSection groupCooldowns = configuration.getConfigurationSection("group-cooldowns");
        if (groupCooldowns != null) {
            for (String cooldownName : groupCooldowns.getKeys(false)) {
                GroupCooldown groupCooldown = new GroupCooldown(cooldownName, groupCooldowns.getInt(cooldownName, defaultItemCooldown));
                ItemIndexer.registerGroupCooldown(groupCooldown);
            }
        }

        // give-inventory-on-join-overrides-player-inventory
        giveInventoryOnJoinOverridesPlayerInventory = configuration.getBoolean("give-inventory-on-join-overrides-player-inventory", false);

        // setEmptySlotsWhenOverridingInventory
        setEmptySlotsWhenOverridingInventory = configuration.getBoolean("set-empty-slots-when-overriding-inventory", true);

        // playerVisibilityToggle
        playerVisibilityToggle = configuration.getBoolean("player-visibility-toggle", false);


    }

    private static void loadInventories(ConfigurationSection inventories) throws InventoryParseException {
        if (inventories == null)
            return;
        for (String invName : inventories.getKeys(false)) {

            ConfigurationSection inventorySection = inventories.getConfigurationSection(invName);
            Inventory inventory = InventoryParser.parseInventorySection(inventorySection);
            InventoryManager.instance().addInventory(inventory);
        }

        Logger.info("Successfully parsed 'inventories'");
    }

    private static void loadTriggers(ConfigurationSection triggers) throws TriggerParseException {
        if (triggers == null)
            return;
        for (String triggerName : triggers.getKeys(false)) {

            Trigger trigger = TriggerParser.parseTriggerSection(triggers.getConfigurationSection(triggerName));

            TriggerManager.instance().addTrigger(trigger);
        }

        Logger.info("Successfully parsed 'triggers'");
    }


}
