package com.gmail.val59000mc.simpleinventorygui.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.configuration.ConfigurationParser;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryManager;
import com.gmail.val59000mc.simpleinventorygui.players.PlayersManager;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class SigCommandExecutor implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {
		if(sender instanceof Player){
			Player player = (Player) sender;
			if(args.length>0){
				switch(args[0]){
					case "reload":
						Bukkit.getLogger().info("Reloading SimpleInventoryGUI configuration");
						boolean success = SIG.getPlugin().reload();
						if(success){
							success(player,"Successfully reloaded SimpleInventoryGUI");
						}else{
							error(player,"Errors have occurred while reloading SimpleInventoryGUI, see the console for detailed errors.");
						}
						break;
					default:
						Inventory inv = InventoryManager.instance().getInventoryByName(args[0]);
						if(inv != null){
							SigPlayer sigPlayer = PlayersManager.getSigPlayer(player);
							inv.giveToPlayer(player,sigPlayer,ConfigurationParser.giveInventoryOnJoinOverridesPlayerInventory); // true = override player's inventory
						}else{
							error(player,"There is no inventory called "+args[0]);
						}
						break;
				}
			}
		}
		
		if(sender instanceof CommandSender && args.length==1 && args[0].equals("reload")){
			SIG.getPlugin().reload();
		}
		

		if(args.length==2){
			Inventory inv = InventoryManager.instance().getInventoryByName(args[0]);
			Player player = Bukkit.getPlayer(args[1]);
			if(player == null){
				sender.sendMessage("§c"+args[0]+" isn't online");
				return true;
			}
			
			if(inv == null){
				sender.sendMessage("§cThere is no inventory called "+args[0]);
			}
			
			SigPlayer sigPlayer = PlayersManager.getSigPlayer(player);
			inv.giveToPlayer(player,sigPlayer,ConfigurationParser.giveInventoryOnJoinOverridesPlayerInventory);
		}
		return true;
	}
	
	private void error(Player player, String message){
		player.sendMessage(ChatColor.RED+message);
	}
	
	private void success(Player player, String message){
		player.sendMessage(ChatColor.GREEN+message);
	}

}
