package com.gmail.val59000mc.simpleinventorygui.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.gmail.val59000mc.simpleinventorygui.placeholders.PlaceholderManager;

public class SIGRegisterPlaceholderEvent extends Event{

	private static final HandlerList handlers = new HandlerList();
	
	private PlaceholderManager pm;

	public SIGRegisterPlaceholderEvent(PlaceholderManager pm) {
		this.pm = pm;
	}
	
	public PlaceholderManager getPm() {
		return pm;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
        return handlers;
    }
}
