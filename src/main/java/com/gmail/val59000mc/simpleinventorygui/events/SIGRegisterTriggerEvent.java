package com.gmail.val59000mc.simpleinventorygui.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.gmail.val59000mc.simpleinventorygui.triggers.TriggerManager;

public class SIGRegisterTriggerEvent extends Event{

	private static final HandlerList handlers = new HandlerList();
	
	private TriggerManager tm;

	public SIGRegisterTriggerEvent(TriggerManager tm) {
		this.tm = tm;
	}
	
	public TriggerManager getTm() {
		return tm;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
        return handlers;
    }
}
