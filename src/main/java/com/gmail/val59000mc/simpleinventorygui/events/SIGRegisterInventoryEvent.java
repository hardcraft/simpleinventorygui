package com.gmail.val59000mc.simpleinventorygui.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.gmail.val59000mc.simpleinventorygui.actions.parsers.ActionParserManager;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryManager;

public class SIGRegisterInventoryEvent extends Event{

	private static final HandlerList handlers = new HandlerList();
	
	private InventoryManager im;

	public SIGRegisterInventoryEvent(InventoryManager im) {
		this.im = im;
	}

	public InventoryManager getIm() {
		return im;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
        return handlers;
    }
}
