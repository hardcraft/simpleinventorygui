package com.gmail.val59000mc.simpleinventorygui.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.gmail.val59000mc.simpleinventorygui.actions.parsers.ActionParserManager;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryManager;

public class SIGRegisterConfigurationFileEvent extends Event{

	private static final HandlerList handlers = new HandlerList();

	public SIGRegisterConfigurationFileEvent() {
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
        return handlers;
    }
}
