package com.gmail.val59000mc.simpleinventorygui.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.gmail.val59000mc.simpleinventorygui.actions.parsers.ActionParserManager;

public class SIGRegisterActionParserEvent extends Event{

	private static final HandlerList handlers = new HandlerList();
	
	private ActionParserManager ap;

	public SIGRegisterActionParserEvent(ActionParserManager ap) {
		this.ap = ap;
	}
	
	
	public ActionParserManager getAp() {
		return ap;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
        return handlers;
    }
}
