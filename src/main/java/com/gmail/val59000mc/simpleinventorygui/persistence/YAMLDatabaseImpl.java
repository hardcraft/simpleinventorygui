package com.gmail.val59000mc.simpleinventorygui.persistence;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.actions.toggle.Toggle;
import com.gmail.val59000mc.simpleinventorygui.actions.toggle.ToggleManager;
import com.gmail.val59000mc.simpleinventorygui.actions.toggle.ToggleType;
import com.gmail.val59000mc.simpleinventorygui.exceptions.DatabaseConnectionException;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class YAMLDatabaseImpl implements IDatabase{
	
	FileConfiguration cfg;
	File file;
	
	
	@Override
	public void connect() throws DatabaseConnectionException{
		file = new File(SIG.getPlugin().getDataFolder().getAbsolutePath()+"/db.yml");
		
		cfg = YamlConfiguration.loadConfiguration(file);
		
		UUID defaultUUID = UUID.fromString("00000000-0000-0000-0000-000000000000");
		List<String> toggles = new ArrayList<String>();
		toggles.add("ChatAlertToggle true");
		cfg.addDefault("players."+defaultUUID.toString()+".toggles.CHAT_ALERT",false);
		cfg.addDefault("players."+defaultUUID.toString()+".toggles.PLAYER_VISIBILITY",true);
		cfg.options().copyDefaults(true);
		
		try {
			cfg.save(file);
		} catch (IOException e) {
			throw new DatabaseConnectionException("Cannot write default db.yml file");
		}
		
	}

	@Override
	public SigPlayer getSigPlayer(UUID uuid) {
		Map<ToggleType,Toggle> toggles = new HashMap<ToggleType,Toggle>();
		ConfigurationSection togglesSection = cfg.getConfigurationSection("players."+uuid+".toggles");
		
		if(togglesSection != null){
			for(String toggleKey : togglesSection.getKeys(false)){
				try {
					ToggleType toggleType = ToggleType.valueOf(toggleKey);
					Boolean storedValue = togglesSection.getBoolean(toggleKey);
					toggles.put(toggleType, ToggleManager.newToggle(toggleType, storedValue));
				} catch (IllegalArgumentException e) {
					// Ignore toggle if not exists
				}
			}
		}
		
		return new SigPlayer.Builder(uuid, toggles).build();
	}

	@Override
	public List<SigPlayer> getSigPlayers(List<UUID> uuidList) {
		
		List<SigPlayer> sigPlayers = new ArrayList<SigPlayer>();
		for(UUID uuid : uuidList){
			sigPlayers.add(getSigPlayer(uuid));
		}
		
		return sigPlayers;
	}



	@Override
	public void saveSigPlayers(List<SigPlayer> sigPlayers) {
		for(SigPlayer sigPlayer : sigPlayers){
			for(Entry<ToggleType, Toggle> toggle : sigPlayer.getToggles().entrySet()){
				cfg.set("players."+sigPlayer.getUuid()+".toggles."+toggle.getValue().getType().toString(), toggle.getValue().isActive());	
			}
		}
		try {
			cfg.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void saveSigPlayer(SigPlayer sigPlayer) {
		for(Entry<ToggleType, Toggle> toggle : sigPlayer.getToggles().entrySet()){
			cfg.set("players."+sigPlayer.getUuid()+".toggles."+toggle.getValue().getType().toString(), toggle.getValue().isActive());	
		}
		try {
			cfg.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void disconnect() {
		// TODO Auto-generated method stub
		
	}

}
