package com.gmail.val59000mc.simpleinventorygui.persistence;

import java.util.List;
import java.util.UUID;

import com.gmail.val59000mc.simpleinventorygui.exceptions.DatabaseConnectionException;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public interface IDatabase {
	
	public void connect() throws DatabaseConnectionException;
	
	public SigPlayer getSigPlayer(UUID uuid);
	public List<SigPlayer> getSigPlayers(List<UUID> uuid);
	
	public void saveSigPlayer(SigPlayer sigPlayer);
	public void saveSigPlayers(List<SigPlayer> sigPlayer);
	
	public void disconnect();

}
