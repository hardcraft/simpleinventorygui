package com.gmail.val59000mc.simpleinventorygui.actions.parsers;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.ErrorAction;
import com.gmail.val59000mc.simpleinventorygui.actions.TogglePermissionAction;
import com.gmail.val59000mc.simpleinventorygui.configuration.ConfigurationParser;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ItemParseException;
import com.gmail.val59000mc.simpleinventorygui.items.Item.ItemBuilder;
import com.gmail.val59000mc.simpleinventorygui.items.ItemParser;

public class TogglePermissionActionParser extends ActionParser {

	@Override
	public String getType() {
		return "toggle-permission";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		if(ConfigurationParser.vaultLoaded){
			String nodeStr = action.getString("permission");
			if(nodeStr == null){
				throw new ActionParseException("#The toggle-permission action '"+action.getName()+"' must provide a permission attribute");		
			}
			
			TogglePermissionAction togglePermissionAction = new TogglePermissionAction(nodeStr);
			
			String itemPermissionTrue = action.getString("item-permission-true");
			String itemPermissionFalse = action.getString("item-permission-false");
			if(itemPermissionTrue != null && itemPermissionFalse != null){
				ItemBuilder builderTrue;
				ItemBuilder builderFalse;
				try {
					builderTrue = ItemParser.parseItemString(itemPermissionTrue);
				} catch (ItemParseException e) {
					throw new ActionParseException("#Error parsing item-permission-true attribute of action '"+action.getName()+"' : "+e.getMessage());		
				}
				try {
					builderFalse = ItemParser.parseItemString(itemPermissionFalse);
				} catch (ItemParseException e) {
					throw new ActionParseException("#Error parsing item-permission-false attribute of action '"+action.getName()+"' : "+e.getMessage());		
				}
				togglePermissionAction.setAlternatesItems(builderTrue.build(),builderFalse.build());
			}
			
			return togglePermissionAction;
		}
		return new ErrorAction("Vault must be installed to use toggle permission action");
	}

}
