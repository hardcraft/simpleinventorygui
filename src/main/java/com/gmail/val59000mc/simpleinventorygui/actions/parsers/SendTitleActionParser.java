package com.gmail.val59000mc.simpleinventorygui.actions.parsers;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.ErrorAction;
import com.gmail.val59000mc.simpleinventorygui.actions.SendTitleAction;
import com.gmail.val59000mc.simpleinventorygui.configuration.ConfigurationParser;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public class SendTitleActionParser extends ActionParser {

	@Override
	public String getType() {
		return "send-title";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		if(ConfigurationParser.bountifulApiLoaded){
			String title = action.getString("title");
			if(title == null)
				throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide a 'title' attribute");		
			SendTitleAction.Builder builderTitle = new SendTitleAction.Builder().withTitle(title);
			
			builderTitle.withSubtitle(action.getString("subtitle"));
			builderTitle.withFadeIn(action.getInt("fade-in",10));
			builderTitle.withStay(action.getInt("stay",40));
			builderTitle.withFadeOut(action.getInt("fade-out",10));
			return builderTitle.build();
		}
		return new ErrorAction("BountifulAPI must be installed to perform this "+getType()+" action");
	}

}
