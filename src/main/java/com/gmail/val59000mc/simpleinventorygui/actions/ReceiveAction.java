package com.gmail.val59000mc.simpleinventorygui.actions;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class ReceiveAction extends Action{
	Double price;

	public ReceiveAction(Double price) {
		this.price = price;
	}
	
	@Override
	public void executeAction(Player player, SigPlayer sigPlayer){
		
		
		OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(player.getUniqueId());
		
		Bukkit.getScheduler().runTaskAsynchronously(SIG.getPlugin(), new Runnable() {
			
			@Override
			public void run() {
				executeNextAction(player, sigPlayer, true);
			}
		});
	}
}
