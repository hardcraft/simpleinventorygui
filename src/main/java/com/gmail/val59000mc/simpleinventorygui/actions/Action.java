package com.gmail.val59000mc.simpleinventorygui.actions;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.ActionExecutor;
import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public abstract class Action{		
	
	protected ActionExecutor actionExecutor;
	
	public void setActionExecutor(ActionExecutor actionExecutor){
		this.actionExecutor = actionExecutor;
	}
	
	public abstract void executeAction(Player player, SigPlayer sigPlayer);
	
	protected void executeNextAction(Player player, SigPlayer sigPlayer, boolean success){
		
			Bukkit.getScheduler().runTask(SIG.getPlugin(), new Runnable(){

				@Override
				public void run() {
					Action nextAction = actionExecutor.getNextAction(Action.this);
					if(nextAction == null || !success || !player.isOnline()){
						actionExecutor.endActionsExecution(player,sigPlayer,success);
					}else{
						nextAction.executeAction(player, sigPlayer);
					}
					
				}});
			
			
		}
}
