package com.gmail.val59000mc.simpleinventorygui.actions.toggle;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class ChatAlertToggle extends Toggle {

	public ChatAlertToggle() {
		super(ToggleType.CHAT_ALERT,false);
	}

	@Override
	public void executeToggle(Player player) {
		if(active){
			player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_GUITAR, 1, 2);
		}
	}

}
