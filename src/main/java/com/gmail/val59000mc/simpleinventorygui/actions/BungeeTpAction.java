package com.gmail.val59000mc.simpleinventorygui.actions;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.simpleinventorygui.util.BungeeUtils;

public class BungeeTpAction extends Action{
	
	private String serverName;
	
	public BungeeTpAction(String serverName){
		this.serverName = serverName;
	}

	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {

		BungeeUtils.sendPlayerToServer(player, serverName);
		executeNextAction(player, sigPlayer, true);
	}
}
