package com.gmail.val59000mc.simpleinventorygui.actions;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.placeholders.PlaceholderManager;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class RunConsoleCommandAction extends Action{
	String command;

	public RunConsoleCommandAction(String command) {
		this.command = command;
	}
	
	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
		String cmd = PlaceholderManager.instance().replacePlaceholders(command, player, sigPlayer);
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
		executeNextAction(player, sigPlayer, true);
	}
}
