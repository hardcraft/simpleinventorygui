package com.gmail.val59000mc.simpleinventorygui.actions.parsers;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.SendMessageAction;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public class SendMessageActionParser extends ActionParser {

	@Override
	public String getType() {
		return "send-message";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		String message = action.getString("message");
		if(message == null){
			throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide a 'message' attribute");		
		}
		return new SendMessageAction(message);
	}

}
