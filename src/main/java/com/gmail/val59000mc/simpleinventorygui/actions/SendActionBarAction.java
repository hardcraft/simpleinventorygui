package com.gmail.val59000mc.simpleinventorygui.actions;

import com.gmail.val59000mc.simpleinventorygui.placeholders.PlaceholderManager;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class SendActionBarAction extends Action {
	private String message;
	
	public SendActionBarAction(String message){
		this.message = ChatColor.translateAlternateColorCodes('&', message);
	}
	
	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
        String msg = PlaceholderManager.instance().replacePlaceholders(message, player, sigPlayer);
        player.sendMessage(msg);
		executeNextAction(player, sigPlayer, true);
	}

}
