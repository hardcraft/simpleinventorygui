package com.gmail.val59000mc.simpleinventorygui.actions.parsers;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public abstract class ActionParser {
		
	public abstract String getType();
	
	public abstract Action parseAction(ConfigurationSection action) throws ActionParseException;

}
