package com.gmail.val59000mc.simpleinventorygui.actions;

import java.util.Collection;

import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class PlaySoundAction extends Action {

	private Sound sound;
	private float volume;
	private float pitch;
	private Double radius;
	
	private PlaySoundAction(Builder builder){
		this.sound = builder.sound;
		this.volume = builder.volume;
		this.pitch = builder.pitch;
		this.radius = builder.radius;
	}
	
	public static class Builder{
		private Sound sound;
		private float volume;
		private float pitch;
		private Double radius;
		
		public Builder(){
			this.sound = Sound.BLOCK_NOTE_BLOCK_GUITAR;
			this.volume = 1;
			this.pitch = 1;
			this.radius = null;
		}
		
		public Builder withSound(Sound sound){
			this.sound = sound;
			return this;
		}
		
		public Builder withVolume(float volume){
			this.volume = volume;
			return this;
		}
		
		public Builder withPitch(float pitch){
			this.pitch = pitch;
			return this;
		}
		
		public Builder withRadius(double radius){
			this.radius = radius;
			return this;
		}
		
		public PlaySoundAction build(){
			return new PlaySoundAction(this);
		}
	}
	
	
	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {

		if(radius == null){
			player.playSound(player.getLocation(), sound, volume, pitch);
		}else{
			Collection<Entity> entities = player.getWorld().getNearbyEntities(player.getLocation(), 2*radius, 2*radius, 2*radius);
			for(Entity entity : entities){
				if(entity.getType().equals(EntityType.PLAYER)){
					((Player) entity).playSound(((Player) entity).getLocation(), sound, volume, pitch);
				}
			}
		}
		executeNextAction(player, sigPlayer, true);
	}

}
