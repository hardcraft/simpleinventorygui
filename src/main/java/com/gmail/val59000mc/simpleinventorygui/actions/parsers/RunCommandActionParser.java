package com.gmail.val59000mc.simpleinventorygui.actions.parsers;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.RunCommandAction;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public class RunCommandActionParser extends ActionParser {

	@Override
	public String getType() {
		return "run-command";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		String cmdStr = action.getString("command");
		if(cmdStr == null){
			throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide a 'command' attribute");		
		}
		return new RunCommandAction(cmdStr);
	}

}
