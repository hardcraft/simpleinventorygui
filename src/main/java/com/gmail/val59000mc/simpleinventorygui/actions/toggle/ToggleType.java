package com.gmail.val59000mc.simpleinventorygui.actions.toggle;

public enum ToggleType {
	CHAT_ALERT,
	PLAYER_VISIBILITY;
}
