package com.gmail.val59000mc.simpleinventorygui.actions;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.actions.toggle.ToggleManager;
import com.gmail.val59000mc.simpleinventorygui.actions.toggle.ToggleType;
import com.gmail.val59000mc.simpleinventorygui.configuration.Lang;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.items.ItemModifier;
import com.gmail.val59000mc.simpleinventorygui.players.PlayersManager;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class ToggleOptionAction extends Action implements ItemModifier{

	private ToggleType toggleType;
	private Item itemOn;
	private Item itemOff;
	private boolean displayMessage;
	
	public ToggleOptionAction(ToggleType toggleType, boolean displayMessage){
		this.toggleType = toggleType;
		this.itemOn = null;
		this.itemOff = null;
		this.displayMessage = displayMessage;
	}
	
	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
		boolean newState = ToggleManager.toggleOption(sigPlayer,toggleType);
		PlayersManager.saveSigPlayer(sigPlayer);
		if(displayMessage){
			player.getPlayer().sendMessage(
				Lang.TOGGLE
				.replace("%toggle-state%", ((newState == true) ? "ON " : "OFF "))
				.replace("%toggle-name%", toggleType.toString().replace("_","-").toLowerCase())
			);
		}
		executeNextAction(player, sigPlayer, true);
	}

	public void setAlternatesItems(Item itemOn, Item itemOff) {
		this.itemOn = itemOn;
		this.itemOff = itemOff;
	}
	
	// ItemModifier interface
	
	@Override
	public Item getAlternateVersion(Player player, SigPlayer sigPlayer) {
		return (ToggleManager.getOptionState(sigPlayer, toggleType) == true) ? itemOn : itemOff;
	}

	@Override
	public boolean isModifier() {
		return itemOn != null && itemOff != null;
	}
	
}
