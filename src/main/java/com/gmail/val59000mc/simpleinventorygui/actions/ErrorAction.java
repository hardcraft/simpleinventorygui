package com.gmail.val59000mc.simpleinventorygui.actions;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class ErrorAction extends Action{

	String message;
	
	public ErrorAction(String message) {
		this.message = message;
	}
	
	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
		player.sendMessage(ChatColor.RED+message);		
		executeNextAction(player, sigPlayer, false);
	}

}
