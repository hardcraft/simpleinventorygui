package com.gmail.val59000mc.simpleinventorygui.actions.parsers;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.OpenInventoryAction;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public class OpenInventoryActionParser extends ActionParser {

	@Override
	public String getType() {
		return "open-inventory";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		String openInvStr = action.getString("inventory");
		if(openInvStr == null){
			throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide an 'inventory' attribute");		
		}
		return new OpenInventoryAction(openInvStr);
	}

}
