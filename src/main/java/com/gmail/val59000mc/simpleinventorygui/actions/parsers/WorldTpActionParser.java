package com.gmail.val59000mc.simpleinventorygui.actions.parsers;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.WorldTpAction;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public class WorldTpActionParser extends ActionParser {

	@Override
	public String getType() {
		return "world-tp";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		String locStr = action.getString("location");
		if(locStr == null){
			throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide a 'location' attribute, example 'world:0:60:0:180:0'");		
		}
		String[] locArr = locStr.split(":");
		if(locArr.length != 6){
			throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must respect the format 'world_name:x:y:z:yaw:pitch' , example 'world:0:60:0:180:0'");		
		}
		
		Double x,y,z;
		Float yaw,pitch;
		String world;
		try {
			x = Double.parseDouble(locArr[1]);
			y = Double.parseDouble(locArr[2]);
			z = Double.parseDouble(locArr[3]);
			yaw = Float.parseFloat(locArr[4]);
			pitch = Float.parseFloat(locArr[5]);
		} catch (NumberFormatException e) {
			throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' coordinates must be decimal or integer values, example 'world:0:60:0:180:0'");		
		}
		world = locArr[0];
		
		return new WorldTpAction(world,x,y,z,yaw,pitch);
	}

}
