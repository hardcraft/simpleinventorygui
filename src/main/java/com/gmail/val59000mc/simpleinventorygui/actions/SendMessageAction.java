package com.gmail.val59000mc.simpleinventorygui.actions;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.placeholders.PlaceholderManager;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class SendMessageAction extends Action {

	private String message;
	
	public SendMessageAction(String message){
		this.message = ChatColor.translateAlternateColorCodes('&', message);
	}
	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
		String str = PlaceholderManager.instance().replacePlaceholders(message, player, sigPlayer);
		player.sendMessage(str);
		executeNextAction(player, sigPlayer, true);
	}

}
