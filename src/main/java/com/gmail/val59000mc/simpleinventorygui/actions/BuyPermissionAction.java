package com.gmail.val59000mc.simpleinventorygui.actions;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.configuration.Lang;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.items.ItemModifier;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class BuyPermissionAction extends Action implements ItemModifier {

    private String node;
    private Double price;
    private Item itemPermissionTrue;
    private Item itemPermissionFalse;

    public BuyPermissionAction(String node, Double price) {
        this.node = node;
        this.price = price;
        this.itemPermissionTrue = null;
        this.itemPermissionFalse = null;

    }

    @Override
    public void executeAction(Player player, SigPlayer sigPlayer) {
        if (player.hasPermission(node)) {
            player.sendMessage(Lang.ALREADY_BOUGHT_PERMISSION.replace("%node%", node));
            executeNextAction(player, sigPlayer, false);
            return;
        }
        final String worldname = (player.isOnline()) ? player.getWorld().getName() : null;

        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(player.getUniqueId());

        Bukkit.getScheduler().runTaskAsynchronously(SIG.getPlugin(), new Runnable() {

            @Override
            public void run() {
                // removed vault
            }
        });


    }

    public void setAlternatesItems(Item itemPermissionTrue, Item itemPermissionFalse) {
        this.itemPermissionTrue = itemPermissionTrue;
        this.itemPermissionFalse = itemPermissionFalse;
    }

    // ItemModifier interface

    @Override
    public Item getAlternateVersion(Player player, SigPlayer sigPlayer) {
        return (player.hasPermission(node) ? itemPermissionTrue : itemPermissionFalse);
    }

    @Override
    public boolean isModifier() {
        return itemPermissionTrue != null && itemPermissionFalse != null;
    }

}
