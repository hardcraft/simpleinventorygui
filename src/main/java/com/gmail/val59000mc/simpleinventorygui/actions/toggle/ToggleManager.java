package com.gmail.val59000mc.simpleinventorygui.actions.toggle;

import java.util.Map;

import net.md_5.bungee.api.ChatColor;

import com.gmail.val59000mc.simpleinventorygui.configuration.ConfigurationParser;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class ToggleManager {

	public static Boolean toggleOption(SigPlayer sigPlayer, ToggleType toggleType) {
		
			Map<ToggleType, Toggle> playerToggles = sigPlayer.getToggles();
			Toggle toggle = playerToggles.get(toggleType);
			if(toggle == null){
				toggle = newToggle(toggleType,null);
			}
			if(sigPlayer.isOnline()){
				toggle.toggle(sigPlayer.getPlayer());
				playerToggles.put(toggleType,toggle);
				sigPlayer.setToggles(playerToggles);
			}
			
			return toggle.isActive();
	}
	
	public static boolean getOptionState(SigPlayer sigPlayer, ToggleType toggleType) {
		
		Map<ToggleType, Toggle> playerToggles = sigPlayer.getToggles();
		Toggle toggle = playerToggles.get(toggleType);
		if(toggle == null){
			toggle = newToggle(toggleType,null);
		}
		
		return toggle.isActive();
	}

	public static String getOptionStringState(SigPlayer sigPlayer, ToggleType toggleType) {
		Boolean state = getOptionState(sigPlayer,toggleType);
		return ((state == true) ? ChatColor.GREEN+"ON" : ChatColor.RED+"OFF");
	}
		
	public static Toggle newToggle(ToggleType toggleType, Boolean forceValue){
		Toggle toggle;
		switch(toggleType){
			case PLAYER_VISIBILITY:
				toggle =  new PlayerVisibilityToggle();
				break;
			default: 
			case CHAT_ALERT:
				toggle = new ChatAlertToggle();
				break;
		}
		if(forceValue != null){
			toggle.setState(forceValue);
		}
		return toggle;
	}

	public static void executeTogglePlayerVisibilityFor(SigPlayer sigPlayer) {
		if(ConfigurationParser.playerVisibilityToggle){
			Map<ToggleType, Toggle> playerToggles = sigPlayer.getToggles();
			Toggle toggle = playerToggles.get(ToggleType.PLAYER_VISIBILITY);
			if(toggle == null)
				toggle = newToggle(ToggleType.PLAYER_VISIBILITY,null);
			if(sigPlayer.isOnline()){
				toggle.executeToggle(sigPlayer.getPlayer());
			}
		}
	}

	public static void executeChatAlertFor(SigPlayer sigPlayer) {
		Map<ToggleType, Toggle> playerToggles = sigPlayer.getToggles();
		Toggle toggle = playerToggles.get(ToggleType.CHAT_ALERT);
		if(toggle == null)
			toggle = newToggle(ToggleType.CHAT_ALERT,null);
		if(sigPlayer.isOnline()){
			toggle.executeToggle(sigPlayer.getPlayer());
		}
	}
		
}
