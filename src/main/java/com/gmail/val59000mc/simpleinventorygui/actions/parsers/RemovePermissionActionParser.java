package com.gmail.val59000mc.simpleinventorygui.actions.parsers;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.ErrorAction;
import com.gmail.val59000mc.simpleinventorygui.actions.RemovePermissionAction;
import com.gmail.val59000mc.simpleinventorygui.configuration.ConfigurationParser;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public class RemovePermissionActionParser extends ActionParser {

	@Override
	public String getType() {
		return "remove-permission";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		if(ConfigurationParser.vaultLoaded){
			String nodeStr = action.getString("permission");
			if(nodeStr == null){
				throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide a permission attribute");		
			}
			return new RemovePermissionAction(nodeStr);
		}
		return new ErrorAction("Vault must be installed to use perform this "+getType()+" action");
	}

}
