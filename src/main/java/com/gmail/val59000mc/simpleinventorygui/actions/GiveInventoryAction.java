package com.gmail.val59000mc.simpleinventorygui.actions;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryManager;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class GiveInventoryAction extends Action{
	String targetInventory;
	
	public GiveInventoryAction(String targetInventory){
		this.targetInventory = targetInventory;
	}

	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
		Inventory inv = InventoryManager.instance().getInventoryByName(targetInventory);
		if(inv != null){
			inv.giveToPlayer(player,sigPlayer,true); // true = override player's inventory
		}	
		executeNextAction(player, sigPlayer, true);
	}
}
