package com.gmail.val59000mc.simpleinventorygui.actions;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class CloseInventoryAction extends Action{
	
	public CloseInventoryAction(){
	}

	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
		player.getPlayer().closeInventory();
		executeNextAction(player, sigPlayer, true);
	}
}
