package com.gmail.val59000mc.simpleinventorygui.actions.parsers;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.ErrorAction;
import com.gmail.val59000mc.simpleinventorygui.actions.PayAction;
import com.gmail.val59000mc.simpleinventorygui.configuration.ConfigurationParser;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public class PayActionParser extends ActionParser {

	@Override
	public String getType() {
		return "pay";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		if(ConfigurationParser.vaultLoaded){
			Double price = action.getDouble("price");
			if(price == null){
				throw new ActionParseException("#The "+getType()+"  action '"+action.getName()+"' must provide price attribute");		
			}
			if(price < 0){
				throw new ActionParseException("#The "+getType()+"  action '"+action.getName()+"' must provide a positive price attribute");	
			}
					
			return new PayAction(price);
		}
		return new ErrorAction("Vault must be installed to use perform this pay action");
	}

}
