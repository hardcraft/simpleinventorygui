package com.gmail.val59000mc.simpleinventorygui.actions;

import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.items.ItemModifier;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class TogglePermissionAction extends Action implements ItemModifier {

    private String node;
    private Item itemPermissionTrue;
    private Item itemPermissionFalse;

    public TogglePermissionAction(String node) {
        this.node = node;
        this.itemPermissionTrue = null;
        this.itemPermissionFalse = null;

    }

    @Override
    public void executeAction(Player player, SigPlayer sigPlayer) {
        final String worldname = (player.isOnline()) ? player.getWorld().getName() : null;

        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(player.getUniqueId());

        // removed vault


    }

    public void setAlternatesItems(Item itemPermissionTrue, Item itemPermissionFalse) {
        this.itemPermissionTrue = itemPermissionTrue;
        this.itemPermissionFalse = itemPermissionFalse;
    }

    // ItemModifier interface

    @Override
    public Item getAlternateVersion(Player player, SigPlayer sigPlayer) {
        return (player.hasPermission(node) ? itemPermissionTrue : itemPermissionFalse);
    }

    @Override
    public boolean isModifier() {
        return itemPermissionTrue != null && itemPermissionFalse != null;
    }

}
