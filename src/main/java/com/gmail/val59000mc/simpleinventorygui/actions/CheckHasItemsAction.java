package com.gmail.val59000mc.simpleinventorygui.actions;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.simpleinventorygui.configuration.Lang;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.ItemCompareOption;

public class CheckHasItemsAction extends Action {

	private List<Item> items;


	public CheckHasItemsAction(List<Item> items) {
		this.items = items;
	}
	
	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
		for(Item item : items){
			ItemStack stack = item.buildRawItem(player,sigPlayer);
			
			if(!Inventories.containsAtLeast(player.getInventory(), stack, stack.getAmount(), new ItemCompareOption())){
				player.sendMessage(Lang.NOT_ENOUGH_ITEMS);
				executeNextAction(player, sigPlayer, false);
				return;
			}
		}
		
		executeNextAction(player, sigPlayer, true);
	}

}
