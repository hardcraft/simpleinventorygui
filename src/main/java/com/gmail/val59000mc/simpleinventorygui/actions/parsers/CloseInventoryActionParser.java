package com.gmail.val59000mc.simpleinventorygui.actions.parsers;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.CloseInventoryAction;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public class CloseInventoryActionParser extends ActionParser {

	@Override
	public String getType() {
		return "close-inventory";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		return new CloseInventoryAction();
	}

}
