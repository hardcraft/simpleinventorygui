package com.gmail.val59000mc.simpleinventorygui.actions;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.configuration.Lang;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.ItemCompareOption;

public class SellItemAction extends Action {

	private Item item;
	private Double price;
	private boolean displayMessage;
	
	private ItemCompareOption itemCompareOption;


	public SellItemAction(Item item, Double price, boolean displayMessage) {
		this.item = item;
		this.price = price;
		this.displayMessage = displayMessage;
		this.itemCompareOption = new ItemCompareOption.Builder()
				.withMaterial(true)
				.withDamageValue(true)
				.withEnchantments(true)
				.build();
	}
	
	@Override
	public void executeAction(Player player, SigPlayer sigPlayer){
		ItemStack stack = item.buildRawItem(player,sigPlayer);
		// removed vault
	}

}
