package com.gmail.val59000mc.simpleinventorygui.actions.parsers;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.CheckHasItemsAction;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ItemParseException;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.items.Item.ItemBuilder;
import com.gmail.val59000mc.simpleinventorygui.items.ItemParser;

public class CheckHasItemsActionParser extends ActionParser {

	@Override
	public String getType() {
		return "check-has-items";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		List<Item> items = new ArrayList<Item>();
		
		ConfigurationSection itemsSection = action.getConfigurationSection("items");
		if(itemsSection == null){
			throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide an items section");		
		}
		
		for(String itemSectionKey : itemsSection.getKeys(false)){
			ConfigurationSection itemSection = itemsSection.getConfigurationSection(itemSectionKey);
			String itemStr = itemSection.getString("item");
			if(itemStr == null){
				throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide an item attribute");		
			}
			
			ItemBuilder itemBuilder;
			try {
				itemBuilder = ItemParser.parseItemString(itemStr);
			} catch (ItemParseException e) {
				throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide a valid item attribute");
			}
			
			
			List<String> itemCustomizationStrList = action.getStringList("item-customization");
			if(itemCustomizationStrList != null){
				for(String customizeStr : itemCustomizationStrList){
					try {
						ItemParser.customizeItem(itemBuilder,customizeStr);
					} catch (ItemParseException e) {
						throw new ActionParseException(e.getMessage());
					}
				}
			}
			
			items.add(itemBuilder.build());
			
		}
		
			return new CheckHasItemsAction(items);
	}

}
