package com.gmail.val59000mc.simpleinventorygui.actions.toggle;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class PlayerVisibilityToggle extends Toggle{

	public PlayerVisibilityToggle() {
		super(ToggleType.PLAYER_VISIBILITY,true);
	}

	@Override
	public void executeToggle(Player player) {
		if(active){
			for(Player p : Bukkit.getOnlinePlayers()){
				player.showPlayer(p);
			}
		}else{
			for(Player p : Bukkit.getOnlinePlayers()){
				player.hidePlayer(p);
			}
		}
	}

}
