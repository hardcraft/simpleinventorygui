package com.gmail.val59000mc.simpleinventorygui.actions;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.configuration.Lang;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class RemovePermissionAction extends Action{

	private String node;
	
	public RemovePermissionAction(String node){
		this.node = node;
		
	}
	
	@Override
	public void executeAction(Player player, SigPlayer sigPlayer){
		if(!player.hasPermission(node)){
			player.sendMessage(Lang.COULD_NOT_REMOVE_PERMISSION.replace("%node%", node));
			executeNextAction(player, sigPlayer, false);
			return;
		}
		
		final String worldname = (player.isOnline()) ? player.getWorld().getName()  : null;
		OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(player.getUniqueId());
		
		Bukkit.getScheduler().runTaskAsynchronously(SIG.getPlugin(), new Runnable() {
			
			@Override
			public void run() {
				// removed vault
			}
		});
		
		
	}
	
}
