package com.gmail.val59000mc.simpleinventorygui.actions.parsers;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.GiveInventoryAction;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public class GiveInventoryActionParser extends ActionParser {

	@Override
	public String getType() {
		return "give-inventory";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		String giveInvStr = action.getString("inventory");
		if(giveInvStr == null){
			throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide an 'inventory' attribute");		
		}
		return new GiveInventoryAction(giveInvStr);
	}

}
