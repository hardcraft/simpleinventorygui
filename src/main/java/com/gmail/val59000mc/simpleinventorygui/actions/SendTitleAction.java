package com.gmail.val59000mc.simpleinventorygui.actions;

import com.gmail.val59000mc.simpleinventorygui.placeholders.PlaceholderManager;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class SendTitleAction extends Action {
    private String title;
    private String subtitle;
    private int fadeIn;
    private int stay;
    private int fadeOut;

    private SendTitleAction(Builder builder) {
        this.title = ChatColor.translateAlternateColorCodes('&', builder.title);
        this.subtitle = (builder.subtitle == null) ? null : ChatColor.translateAlternateColorCodes('&', builder.subtitle);
        this.stay = builder.stay;
        this.fadeIn = builder.fadeIn;
        this.fadeOut = builder.fadeOut;
    }

    public static class Builder {
        private String title;
        private String subtitle;
        private int fadeIn;
        private int stay;
        private int fadeOut;

        public Builder() {
            this.title = "";
            this.subtitle = null;
            this.stay = 40;
            this.fadeIn = 10;
            this.fadeOut = 10;
        }

        public Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder withSubtitle(String subtitle) {
            this.subtitle = subtitle;
            return this;
        }


        public Builder withFadeIn(int fadeIn) {
            this.fadeIn = fadeIn;
            return this;
        }

        public Builder withStay(int stay) {
            this.stay = stay;
            return this;
        }

        public Builder withFadeOut(int fadeOut) {
            this.fadeOut = fadeOut;
            return this;
        }

        public SendTitleAction build() {
            return new SendTitleAction(this);
        }
    }

    @Override
    public void executeAction(Player player, SigPlayer sigPlayer) {
        String top = (title == null) ? title : PlaceholderManager.instance().replacePlaceholders(title, player, sigPlayer);
        String bottom = (subtitle == null) ? subtitle : PlaceholderManager.instance().replacePlaceholders(subtitle, player, sigPlayer);
        player.sendMessage(top);
        player.sendMessage(bottom);
        executeNextAction(player, sigPlayer, true);
    }

}
