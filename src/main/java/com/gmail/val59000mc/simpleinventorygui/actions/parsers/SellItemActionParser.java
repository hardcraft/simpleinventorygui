package com.gmail.val59000mc.simpleinventorygui.actions.parsers;

import java.util.List;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.ErrorAction;
import com.gmail.val59000mc.simpleinventorygui.actions.SellItemAction;
import com.gmail.val59000mc.simpleinventorygui.configuration.ConfigurationParser;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ItemParseException;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.items.Item.ItemBuilder;

import com.gmail.val59000mc.simpleinventorygui.items.ItemParser;

public class SellItemActionParser extends ActionParser {

	@Override
	public String getType() {
		return "sell-item";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		if(ConfigurationParser.vaultLoaded){
			String buyItemStr = action.getString("item");
			if(buyItemStr == null){
				throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide an item attribute");		
			}
			
			ItemBuilder itemBuilder;
			try {
				itemBuilder = ItemParser.parseItemString(buyItemStr);
			} catch (ItemParseException e) {
				throw new ActionParseException("#The "+getType()+"  action '"+action.getName()+"' must provide a valid item attribute");
			}
			
			
			List<String> itemCustomizationStrList = action.getStringList("item-customization");
			if(itemCustomizationStrList != null){
				for(String customizeStr : itemCustomizationStrList){
					try {
						ItemParser.customizeItem(itemBuilder,customizeStr);
					} catch (ItemParseException e) {
						throw new ActionParseException(e.getMessage());
					}
				}
			}
			
			Item item = itemBuilder.build();
			
			
			Double price = action.getDouble("price");
			if(price < 0){
				throw new ActionParseException("#The "+getType()+"  action '"+action.getName()+"' must provide a positive price attribute");	
			}

			boolean displayMessage = action.getBoolean("display-message", true);
			
			return new SellItemAction(item,price,displayMessage);
		}
		return new ErrorAction("Vault must be installed to use buy/sell action");
	}

}
