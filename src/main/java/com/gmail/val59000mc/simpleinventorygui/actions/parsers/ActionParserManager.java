package com.gmail.val59000mc.simpleinventorygui.actions.parsers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public class ActionParserManager {
	private static ActionParserManager instance;
	
	private Map<String,ActionParser> actionsParsers;
	
	public static void reset(){
		instance = new ActionParserManager();
		instance.actionsParsers = Collections.synchronizedMap(new HashMap<String,ActionParser>());
		instance.registerSigNativeActions();
	}
	
	public static ActionParserManager instance(){
		return instance;
	}
	
	private void registerSigNativeActions() {
		registerActionParser(new BungeeTpActionParser());
		registerActionParser(new BuyItemActionParser());
		registerActionParser(new BuyPermissionActionParser());
		registerActionParser(new CheckHasItemsActionParser());
		registerActionParser(new CloseInventoryActionParser());
		registerActionParser(new GiveInventoryActionParser());
		registerActionParser(new OpenInventoryActionParser());
		registerActionParser(new PayActionParser());
		registerActionParser(new PlaySoundActionParser());
		registerActionParser(new ReceiveActionParser());
		registerActionParser(new RemovePermissionActionParser());
		registerActionParser(new RunCommandActionParser());
		registerActionParser(new RunConsoleCommandActionParser());
		registerActionParser(new SellItemActionParser());
		registerActionParser(new SendActionBarActionParser());
		registerActionParser(new SendMessageActionParser());
		registerActionParser(new SendTitleActionParser());
		registerActionParser(new ToggleOptionActionParser());
		registerActionParser(new TogglePermissionActionParser());
		registerActionParser(new WorldTpActionParser());
		
	}

	public void registerActionParser(ActionParser actionParser){
		if(actionsParsers.containsKey(actionParser.getType())){
			throw new IllegalArgumentException("Action of type '"+actionParser.getType()+"' already exists");
		}
		
		actionsParsers.put(actionParser.getType(), actionParser);
	}
	
	public Action parseActionSection(ConfigurationSection section) throws ActionParseException{
		String type = section.getString("type");
		if(type == null || !actionsParsers.containsKey(type)){
			throw new ActionParseException("#The action '"+section.getName()+"' must have a valid 'type'");
		}
		return actionsParsers.get(type).parseAction(section);
	}
	

	public List<Action> parseAllActionsSection(ConfigurationSection allActions) throws ActionParseException{
		List<Action> actions = new ArrayList<Action>();
		if(allActions == null){
			return actions;
		}
		for(String actionName : allActions.getKeys(false)){
			Action action = parseActionSection(allActions.getConfigurationSection(actionName));
			actions.add(action);
		}
		return actions;
	}
	
}
