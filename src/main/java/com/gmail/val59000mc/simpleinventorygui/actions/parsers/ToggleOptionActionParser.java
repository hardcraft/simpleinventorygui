package com.gmail.val59000mc.simpleinventorygui.actions.parsers;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.ToggleOptionAction;
import com.gmail.val59000mc.simpleinventorygui.actions.toggle.ToggleType;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ItemParseException;
import com.gmail.val59000mc.simpleinventorygui.items.Item.ItemBuilder;
import com.gmail.val59000mc.simpleinventorygui.items.ItemParser;

public class ToggleOptionActionParser extends ActionParser {

	@Override
	public String getType() {
		return "toggle-option";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		String optionStr = action.getString("option").replace("-", "_");
		if(optionStr == null){
			throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide an 'option' attribute");	
		}
		ToggleType toggleType;
		try {
			toggleType = ToggleType.valueOf(optionStr.toUpperCase());
		} catch (IllegalArgumentException e1) {
			throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide a valid 'option' attribute");	
		}	

		boolean displayMessage = action.getBoolean("display-message", true);

		ToggleOptionAction toggleOptionAction = new ToggleOptionAction(toggleType, displayMessage);
		
		
		String itemOn = action.getString("item-on");
		String itemOff = action.getString("item-off");
		if(itemOn != null && itemOff != null){
			ItemBuilder builderOn;
			ItemBuilder builderOff;
			try {
				builderOn = ItemParser.parseItemString(itemOn);
			} catch (ItemParseException e) {
				throw new ActionParseException("#Error parsing item-on attribute of action '"+action.getName()+"' : "+e.getMessage());		
			}
			try {
				builderOff = ItemParser.parseItemString(itemOff);
			} catch (ItemParseException e) {
				throw new ActionParseException("#Error parsing item-off attribute of action '"+action.getName()+"' : "+e.getMessage());		
			}
			toggleOptionAction.setAlternatesItems(builderOn.build(),builderOff.build());
		}
		
		return toggleOptionAction;
	}

}
