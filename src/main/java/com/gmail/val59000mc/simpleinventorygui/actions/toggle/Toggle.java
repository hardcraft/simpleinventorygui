package com.gmail.val59000mc.simpleinventorygui.actions.toggle;

import org.bukkit.entity.Player;

public abstract class Toggle {
	private ToggleType type;
	protected boolean active;
	
	public Toggle(ToggleType type, boolean defaultState){
		this.type = type;
		this.active = defaultState;
	}

	public ToggleType getType() {
		return type;
	}

	public boolean isActive() {
		return active;
	}
	
	public boolean toggle(Player player) {
		this.active = !active;
		executeToggle(player);
		return active;
	}
	
	public abstract void executeToggle(Player player);

	public void setState(Boolean forceValue) {
		this.active = forceValue;
	}
}
