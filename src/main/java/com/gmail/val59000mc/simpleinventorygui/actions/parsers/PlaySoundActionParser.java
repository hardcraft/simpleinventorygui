package com.gmail.val59000mc.simpleinventorygui.actions.parsers;

import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.PlaySoundAction;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public class PlaySoundActionParser extends ActionParser {

	@Override
	public String getType() {
		return "play-sound";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		String soundStr = action.getString("sound");
		if(soundStr == null){
			throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide a 'sound' attribute");		
		}
		Sound sound;
		try {
			sound = Sound.valueOf(soundStr.toUpperCase());
		} catch (IllegalArgumentException e1) {
			throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide a valid 'sound' attribute. See the list : https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/Sound.html");	
		}	
		PlaySoundAction.Builder builderSound = new PlaySoundAction.Builder().withSound(sound);
		
		String volumeStr = action.getString("volume");
		if(volumeStr != null){
			try{
				float volume = Float.parseFloat(volumeStr);
				builderSound.withVolume(volume);
			}catch(NumberFormatException e){
				throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide a valid 'volume' (a number).");
			}
		}
		
		String pitchStr = action.getString("pitch");
		if(pitchStr != null){
			try{
				float pitch = Float.parseFloat(pitchStr);
				builderSound.withPitch(pitch);
			}catch(NumberFormatException e){
				throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide a valid 'pitch' (a number).");
			}
		}
		
		String radiusStr = action.getString("radius");
		if(radiusStr != null){
			try{
				double radius = Double.parseDouble(radiusStr);
				if(radius <= 0 || radius > 39){
					throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide a valid 'radius' (a positive number lower than 39).");
				}
				builderSound.withRadius(radius);
			}catch(NumberFormatException e){
				throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide a valid 'radius' (a positive number lower than 39).");
			}
		}
		
		return builderSound.build();				
	}

}
