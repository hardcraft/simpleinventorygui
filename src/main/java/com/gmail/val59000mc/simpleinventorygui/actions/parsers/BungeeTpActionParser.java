package com.gmail.val59000mc.simpleinventorygui.actions.parsers;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.BungeeTpAction;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public class BungeeTpActionParser extends ActionParser {

	@Override
	public String getType() {
		return "bungee-tp";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		String server = action.getString("server");
		if(server == null){
			throw new ActionParseException("#The bungee-tp action '"+action.getName()+"' must provide a server attribute");		
		}
		return new BungeeTpAction(server);
	}

}
