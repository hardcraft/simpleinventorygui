package com.gmail.val59000mc.simpleinventorygui.actions;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.configuration.Lang;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionInterruptedException;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class WorldTpAction extends Action{

	Double x,y,z;
	Float yaw, pitch;
	String world;
	
	public WorldTpAction(String world, double x, double y, double z,float yaw, float pitch) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = yaw;
		this.pitch = pitch;
		this.world = world;
	}

	@Override
	public void executeAction(Player player, SigPlayer sigPlayer){
		World bukkitWorld = Bukkit.getWorld(world);
		if(bukkitWorld == null){
			player.sendMessage(Lang.COULD_NOT_FIND_WORLD.replace("%world%", world));
			executeNextAction(player, sigPlayer, false);
			return;
		}
		
		player.getPlayer().teleport(new Location(bukkitWorld,x,y,z,yaw,pitch));
		executeNextAction(player, sigPlayer, true);
	}

}
