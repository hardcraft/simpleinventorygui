package com.gmail.val59000mc.simpleinventorygui.actions.parsers;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.ErrorAction;
import com.gmail.val59000mc.simpleinventorygui.actions.SendActionBarAction;
import com.gmail.val59000mc.simpleinventorygui.configuration.ConfigurationParser;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public class SendActionBarActionParser extends ActionParser {

	@Override
	public String getType() {
		return "send-action-bar";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		if(ConfigurationParser.bountifulApiLoaded){
			String actionBarMsg = action.getString("message");
			if(actionBarMsg == null)
				throw new ActionParseException("#The "+getType()+" action '"+action.getName()+"' must provide a 'message' attribute");		
			return new SendActionBarAction(actionBarMsg);
		}
		return new ErrorAction("BountifulAPI must be installed to perform this "+getType()+" action");
	}

}
