package com.gmail.val59000mc.simpleinventorygui.listeners;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.util.BungeeUtils;
import com.google.common.collect.Maps;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

public class BungeeManager implements PluginMessageListener {

	private static BungeeManager instance;
	
	private Map<String, BungeeServerInfo> servers = Maps.newConcurrentMap();
	private boolean run = true;
	
	public static BungeeManager getInstance(){
		return instance;
	}
	
	public BungeeManager(){
		instance = this;
		Bukkit.getScheduler().runTaskAsynchronously(SIG.getPlugin(), new Runnable() {
			
			@Override
			public void run() {
				BungeeUtils.sendMessage("GetServers");
				if(run){
					Bukkit.getScheduler().runTaskLaterAsynchronously(SIG.getPlugin(), this, 200);
				}
			}
		});
	}
	
	public void stop(){
		this.run = false;
	}
	
	public int getPlayerCount(String serverName){
		if(servers.containsKey(serverName)){
			return servers.get(serverName).getPlayerCount();
		}else{
			return new BungeeServerInfo(serverName).getPlayerCount();
		}
	}
	
	public int getPlayerCount(){
		int count = 0;
		for(Entry<String, BungeeServerInfo> info : servers.entrySet()){
			count += getPlayerCount(info.getKey());
		}
		return count;
	}
	
	public class BungeeServerInfo{
		private final String name;
	    private int playerCount = 0;
	    
	    public BungeeServerInfo(final String name) {
	        this.name = name;
	    }

	    public void update() {
	        BungeeUtils.sendMessage(BungeeSubChannel.PlayerCount.toString(),name);
	    } 
	    
	    public String getName() {
	        return name;
	    }

	    public void setPlayerCount(int playerCount) {
	        this.playerCount = playerCount;
	    }

	    public int getPlayerCount() {
	        return playerCount;
	    }
	}
	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] message) {
		
		try {
			
			ByteArrayDataInput in = ByteStreams.newDataInput(message);
			String subChannelStr = in.readUTF();
			if(subChannelStr.equals("GetServer")){
				return;
			}
			
			switch(BungeeSubChannel.valueOf(subChannelStr)){
				case GetServers:
					handleGetServers(in);
					break;
				case PlayerCount:
					handlePlayerCount(in);
					break;
			}
				
			}catch(IllegalArgumentException | IllegalStateException e){
				// ignore malformed message
			}
		
	}
	
	/*
	private DataInputStream readArguments(ByteArrayDataInput in){
		short argumentsLength = in.readShort();
		byte[] argumentsBytes = new byte[argumentsLength];
		in.readFully(argumentsBytes);
		return new DataInputStream(new ByteArrayInputStream(argumentsBytes));
	}
	*/
	
	private void handleGetServers(ByteArrayDataInput in) {
		final List<String> newServers = Arrays.asList(in.readUTF().split(", "));
		
		for (final String server : newServers) {
            servers.put(server, new BungeeServerInfo(server));
            servers.get(server).update();
        }
		
	}
	
	private void handlePlayerCount(ByteArrayDataInput in) {
		
			final String server = in.readUTF();
	        final int playerCount = in.readInt();

	        if (!servers.containsKey(server)) {
	            servers.put(server, new BungeeServerInfo(server));
	        }

	        servers.get(server).setPlayerCount(playerCount);
	}

	

}
