package com.gmail.val59000mc.simpleinventorygui.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.actions.toggle.ToggleManager;
import com.gmail.val59000mc.simpleinventorygui.players.PlayersManager;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class PlayerChatListener implements Listener, ISmartBukkitListener {
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onTalkInCHat(AsyncPlayerChatEvent event){
		
		for(Player player : Bukkit.getOnlinePlayers()){
			String message = event.getMessage();
			if(message.contains(player.getName())){
				SigPlayer sigPlayer = PlayersManager.getSigPlayer(player);
				ToggleManager.executeChatAlertFor(sigPlayer);
			}
		}
		
	
	}

	// ISmartBukkitListener
	@Override
	public void unregister() {
		AsyncPlayerChatEvent.getHandlerList().unregister(this);
	}
	
	@Override
	public void register() {
		Bukkit.getPluginManager().registerEvents(this, SIG.getPlugin());
	}
	
	
}
