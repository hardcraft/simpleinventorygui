package com.gmail.val59000mc.simpleinventorygui.listeners;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.triggers.TriggerManager;
import com.gmail.val59000mc.simpleinventorygui.triggers.TriggerTarget;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import java.util.UUID;

public class PlayerInteractEvent implements Listener, ISmartBukkitListener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInteract(org.bukkit.event.player.PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Block block = event.getClickedBlock();
            if (org.bukkit.block.data.type.Sign.class.equals(block.getBlockData())) {
                TriggerTarget triggerTarget = new TriggerTarget((Sign) block.getState());
                UUID playerUuid = event.getPlayer().getUniqueId();
                boolean triggersFound = TriggerManager.instance().executeTriggers(playerUuid, triggerTarget);
                if (triggersFound) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInteractAtEntity(PlayerInteractEntityEvent event) {

        UUID playerUuid = event.getPlayer().getUniqueId();
        Entity entity = event.getRightClicked();
        TriggerTarget triggerTarget = new TriggerTarget(entity);
        boolean triggersFound = TriggerManager.instance().executeTriggers(playerUuid, triggerTarget);
        if (triggersFound) {
            event.setCancelled(true);
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInteractAtEntity(PlayerInteractAtEntityEvent event) {
        if (event.getRightClicked().getType().equals(EntityType.ARMOR_STAND)) {
            UUID playerUuid = event.getPlayer().getUniqueId();
            Entity entity = event.getRightClicked();
            TriggerTarget triggerTarget = new TriggerTarget(entity);
            boolean triggersFound = TriggerManager.instance().executeTriggers(playerUuid, triggerTarget);
            if (triggersFound) {
                event.setCancelled(true);
            }
        }

    }

    @Override
    public void unregister() {
        PlayerInteractEntityEvent.getHandlerList().unregister(this);
        PlayerInteractAtEntityEvent.getHandlerList().unregister(this);
    }

    @Override
    public void register() {
        Bukkit.getPluginManager().registerEvents(this, SIG.getPlugin());
    }

}
