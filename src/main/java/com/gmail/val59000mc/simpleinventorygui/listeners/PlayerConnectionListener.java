package com.gmail.val59000mc.simpleinventorygui.listeners;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.actions.toggle.ToggleManager;
import com.gmail.val59000mc.simpleinventorygui.configuration.ConfigurationParser;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryManager;
import com.gmail.val59000mc.simpleinventorygui.players.PlayersManager;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class PlayerConnectionListener implements Listener , ISmartBukkitListener{
	
	private static Random r = new Random();
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent event){
		
			final UUID uuid = event.getPlayer().getUniqueId();
			
			final List<UUID> otherOnlineUUID = new ArrayList<UUID>();
			for(Player p : Bukkit.getOnlinePlayers()){
				
				if(!p.equals(event.getPlayer()))
					otherOnlineUUID.add(p.getUniqueId());
			}
			
			Bukkit.getScheduler().runTaskAsynchronously(SIG.getPlugin(), new Runnable() {
				
				@Override
				public void run() {
					
					final SigPlayer sigPlayer = PlayersManager.getSigPlayer(uuid);

					Bukkit.getScheduler().runTaskLater(SIG.getPlugin(), new Runnable() {
						
						@Override
						public void run() {
							// Update player visibility on player join
							ToggleManager.executeTogglePlayerVisibilityFor(sigPlayer);
							
							
							// Default join inventory
							if(ConfigurationParser.giveInventoryOnJoin != "false" && sigPlayer.isOnline()){
								Inventory inv = InventoryManager.instance().getInventoryByName(ConfigurationParser.giveInventoryOnJoin);
								if(inv != null){
									inv.giveToPlayer(sigPlayer.getPlayer(),sigPlayer,ConfigurationParser.giveInventoryOnJoinOverridesPlayerInventory);
								}
							}
							
						}
					}, 5);
					
				}
			});
			
	}

	// ISmartBukkitListener
	@Override
	public void unregister() {
		PlayerJoinEvent.getHandlerList().unregister(this);
	}
	
	@Override
	public void register() {
		Bukkit.getPluginManager().registerEvents(this, SIG.getPlugin());
	}
	
}
