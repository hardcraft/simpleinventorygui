package com.gmail.val59000mc.simpleinventorygui.listeners;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.configuration.ConfigurationParser;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.items.ItemIndexer;
import com.gmail.val59000mc.simpleinventorygui.players.PlayersManager;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class InventoryListener implements Listener, ISmartBukkitListener {

    public InventoryListener() {
        interactActions = new HashSet<org.bukkit.event.block.Action>();
        interactActions.add(org.bukkit.event.block.Action.RIGHT_CLICK_AIR);
        interactActions.add(org.bukkit.event.block.Action.RIGHT_CLICK_BLOCK);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onClickInInventory(InventoryClickEvent event) {

        if (event.getWhoClicked() instanceof Player) {

            UUID playerUuid = event.getWhoClicked().getUniqueId();
            ItemStack hand = event.getCurrentItem();

            final Item item = ItemIndexer.findItem(hand);
            if (item != null) {
                event.setCancelled(true);
                Bukkit.getScheduler().runTaskAsynchronously(SIG.getPlugin(), new Runnable() {

                    @Override
                    public void run() {
                        final SigPlayer sigPlayer = PlayersManager.getSigPlayer(playerUuid);

                        Bukkit.getScheduler().runTask(SIG.getPlugin(), new Runnable() {

                            @Override
                            public void run() {
                                item.executeAllActionsAsynchronously(playerUuid, sigPlayer);
                            }

                        });
                    }

                });
            }
        }
    }


    private static Set<org.bukkit.event.block.Action> interactActions;

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onUseItem(PlayerInteractEvent event) {
        org.bukkit.event.block.Action action = event.getAction();

        if (interactActions.contains(action)) {
            Player player = event.getPlayer();
            UUID playerUuid = player.getUniqueId();
            ItemStack hand = player.getInventory().getItemInMainHand();
            final Item item = ItemIndexer.findItem(hand);
            if (item != null) {

                switch (hand.getType()) {
                    case WRITTEN_BOOK:
                    case WRITABLE_BOOK:
                        // do not cancel
                        break;
                    default:
                        event.setCancelled(true);
                }

                Bukkit.getScheduler().runTaskAsynchronously(SIG.getPlugin(), new Runnable() {

                    @Override
                    public void run() {
                        final SigPlayer sigPlayer = PlayersManager.getSigPlayer(playerUuid);

                        Bukkit.getScheduler().runTask(SIG.getPlugin(), new Runnable() {

                            @Override
                            public void run() {
                                item.executeAllActionsAsynchronously(playerUuid, sigPlayer);
                            }

                        });
                    }

                });
            }
        }

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDropItem(PlayerDropItemEvent event) {
        ItemStack hand = event.getItemDrop().getItemStack();
        Item item = ItemIndexer.findItem(hand);
        if (item != null && !ConfigurationParser.canThrowItems) {
            event.setCancelled(true);
        }
    }

    // ISmartBukkitListener
    @Override
    public void unregister() {
        InventoryClickEvent.getHandlerList().unregister(this);
        PlayerInteractEvent.getHandlerList().unregister(this);
        PlayerDropItemEvent.getHandlerList().unregister(this);
    }

    @Override
    public void register() {
        Bukkit.getPluginManager().registerEvents(this, SIG.getPlugin());
    }


}
