package com.gmail.val59000mc.simpleinventorygui.placeholders;

import java.util.regex.Pattern;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public abstract class RegexPlaceholder implements IPlaceholder{
	
	private Pattern pattern;
	
	public RegexPlaceholder(String pattern){
		this.setPattern(Pattern.compile(pattern));
	}

	public abstract String replacePlaceholder(String original, Player player, SigPlayer sigPlayer);

	public Pattern getPattern() {
		return pattern;
	}

	public void setPattern(Pattern pattern) {
		this.pattern = pattern;
	}

}
