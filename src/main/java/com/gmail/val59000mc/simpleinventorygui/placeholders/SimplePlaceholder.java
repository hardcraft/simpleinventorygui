package com.gmail.val59000mc.simpleinventorygui.placeholders;

import java.util.regex.Pattern;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public abstract class SimplePlaceholder implements IPlaceholder{
	
	private String pattern;
	
	public SimplePlaceholder(String pattern){
		this.setPattern(Pattern.quote(pattern));
	}

	public abstract String replacePlaceholder(String original, Player player, SigPlayer sigPlayer);

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

}
