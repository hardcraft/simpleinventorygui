package com.gmail.val59000mc.simpleinventorygui.placeholders;

import com.gmail.val59000mc.simpleinventorygui.actions.toggle.ToggleManager;
import com.gmail.val59000mc.simpleinventorygui.actions.toggle.ToggleType;
import com.gmail.val59000mc.simpleinventorygui.listeners.BungeeManager;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;

public class PlaceholderManager {
    private static PlaceholderManager instance;

    private Set<IPlaceholder> placeholders;

    public static void reset() {
        instance = new PlaceholderManager();
        instance.loadSIGInternalPlaceholders();
    }

    public static PlaceholderManager instance() {
        return instance;
    }

    private PlaceholderManager() {
        placeholders = Collections.synchronizedSet(new HashSet<IPlaceholder>());

    }


    public void registerNewPlaceholder(IPlaceholder placeholder) {
        placeholders.add(placeholder);
    }

    public void registerNewPlaceholder(String original, String replaceWith) {

        registerNewPlaceholder(new SimplePlaceholder(original) {

            @Override
            public String replacePlaceholder(String original, Player player, SigPlayer sigPlayer) {
                return original.replaceAll(getPattern(), replaceWith);
            }

        });
    }

    public String replacePlaceholders(String original, Player player, SigPlayer sigPlayer) {
        String newString = original;
        synchronized (placeholders) {
            for (IPlaceholder p : placeholders) {
                newString = p.replacePlaceholder(newString, player, sigPlayer);
            }
        }
        return newString;
    }


    private void loadSIGInternalPlaceholders() {

        placeholders.add(new SimplePlaceholder("{player}") {
            @Override
            public String replacePlaceholder(String original, Player player, SigPlayer sigPlayer) {
                return original.replaceAll(getPattern(), player.getName());
            }
        });

        placeholders.add(new SimplePlaceholder("{option.player-visibility}") {
            @Override
            public String replacePlaceholder(String original, Player player, SigPlayer sigPlayer) {
                return original.replaceAll(getPattern(), ToggleManager.getOptionStringState(sigPlayer, ToggleType.PLAYER_VISIBILITY));
            }
        });

        placeholders.add(new SimplePlaceholder("{option.chat-alert}") {
            @Override
            public String replacePlaceholder(String original, Player player, SigPlayer sigPlayer) {
                return original.replaceAll(getPattern(), ToggleManager.getOptionStringState(sigPlayer, ToggleType.CHAT_ALERT));
            }
        });

        placeholders.add(new SimplePlaceholder("{vault.economy.balance}") {
            @Override
            public String replacePlaceholder(String original, Player player, SigPlayer sigPlayer) {
                return original;
            }
        });

        placeholders.add(new SimplePlaceholder("{bungee.playercount}") {
            @Override
            public String replacePlaceholder(String original, Player player, SigPlayer sigPlayer) {
                return original.replaceAll(getPattern(), String.valueOf(BungeeManager.getInstance().getPlayerCount()));
            }
        });

        placeholders.add(new RegexPlaceholder("\\{bungee\\.playercount\\..+\\}") {
            @Override
            public String replacePlaceholder(String original, Player player, SigPlayer sigPlayer) {
                Matcher matcher = getPattern().matcher(original);
                if (matcher.find()) {
                    String totalServerName = original.substring(matcher.start() + 20, matcher.end() - 1);
                    String[] serverNames = totalServerName.split(";");
                    int online = 0;
                    for (String serverName : serverNames) {
                        online += BungeeManager.getInstance().getPlayerCount(serverName);
                    }
                    original = matcher.replaceAll(String.valueOf(online));
                }

                return original;

            }
        });
    }
}
