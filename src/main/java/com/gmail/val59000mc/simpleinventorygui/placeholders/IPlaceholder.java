package com.gmail.val59000mc.simpleinventorygui.placeholders;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public interface IPlaceholder {

	public abstract String replacePlaceholder(String original, Player player, SigPlayer sigPlayer);
}
