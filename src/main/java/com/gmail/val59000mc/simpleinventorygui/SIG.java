package com.gmail.val59000mc.simpleinventorygui;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.simpleinventorygui.api.SIGApi;
import com.gmail.val59000mc.simpleinventorygui.commands.SigCommandExecutor;
import com.gmail.val59000mc.simpleinventorygui.configuration.ConfigurationParser;
import com.gmail.val59000mc.simpleinventorygui.configuration.Lang;
import com.gmail.val59000mc.simpleinventorygui.exceptions.DatabaseConnectionException;
import com.gmail.val59000mc.simpleinventorygui.listeners.BungeeManager;
import com.gmail.val59000mc.simpleinventorygui.listeners.ISmartBukkitListener;
import com.gmail.val59000mc.simpleinventorygui.listeners.InventoryListener;
import com.gmail.val59000mc.simpleinventorygui.listeners.PlayerChatListener;
import com.gmail.val59000mc.simpleinventorygui.listeners.PlayerConnectionListener;
import com.gmail.val59000mc.simpleinventorygui.listeners.PlayerInteractEvent;
import com.gmail.val59000mc.simpleinventorygui.persistence.IDatabase;
import com.gmail.val59000mc.simpleinventorygui.players.PlayersManager;
import com.gmail.val59000mc.simpleinventorygui.util.ReflectionUtils;
import com.gmail.val59000mc.simpleinventorygui.util.ReflectionUtils.PackageType;
import com.gmail.val59000mc.spigotutils.Logger;


public class SIG extends JavaPlugin {

	private static SIG plugin;
	private IDatabase database;
	private BungeeManager bungeeManager;
	private SIGApi api;
	
	private static List<ISmartBukkitListener> listeners;

	public void onEnable(){
		Bukkit.getScheduler().runTaskLater(this, new Runnable() {
			
			@Override
			public void run() {
				enable();
			}
		}, 1);
	}
	
	public boolean reload(){
		onDisable();
		Bukkit.getLogger().info("[SimpleInventoryGUI] Reloading ... ");
		return enable();
	}
	
	public boolean enable(){
		plugin = this;
		
		Logger.setColoredPrefix("§f[§aSimpleInventoryGUI§f]§r ");
		Logger.setStrippedPrefix("[SimpleInventoryGUI] ");
		
		createDirectories();
		saveDefaultConfig();

		listeners = new ArrayList<ISmartBukkitListener>();

		// Register Commands
		getCommand("sig").setExecutor(new SigCommandExecutor());	
		
		boolean parsed = ConfigurationParser.loadConfiguration();
		Lang.loadLang();
		
		// Register bungee channel
		try {
			bungeeManager = new BungeeManager();
			this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
			this.getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", bungeeManager);
		} catch (IllegalArgumentException e) {
			Bukkit.getLogger().info("[SimpleInventoryGUI] Not a BungeeCord instance - bungee-tp actions will fail.");
		}
		
		if(parsed){
			// Register Listeners
			listeners.add(new InventoryListener());
			listeners.add(new PlayerConnectionListener());
			listeners.add(new PlayerChatListener());
			listeners.add(new PlayerInteractEvent());
			for(ISmartBukkitListener listener : listeners){
				listener.register();
			}
			
			// Set API	
			api = new SIGApi();
			return true;
		}else{
			disablePlugin("Wrong configuration, see above.");
			return false;
		}
	}
	
	public static SIG getPlugin(){
		return plugin;
	}
	
	public SIGApi getAPI(){
		return api;
	}
	
	
	
	public void onDisable(){
		Bukkit.getLogger().info("[SimpleInventoryGUI] Unregistering listeners");
		unregisterTaskAndListeners();
		PlayersManager.setDatabase(null);
		closeDatabaseConnection();
	}
	
	private void unregisterTaskAndListeners(){
		// Unregister listeners
		for(ISmartBukkitListener listener : listeners){
			listener.unregister();
		}
		
		// Stop bungee manager
		bungeeManager.stop();
	}
	
	private void createDirectories(){
		if(!plugin.getDataFolder().exists()){
			plugin.getDataFolder().mkdirs();
			copy(getResource("creeper.jpg"), new File(getDataFolder(),"creeper.jpg"));
		}
	}
	
	public void copy(InputStream in, File file) {
	    try {
	        OutputStream out = new FileOutputStream(file);
	        byte[] buf = new byte[1024];
	        int len;
	        while((len=in.read(buf))>0){
	            out.write(buf,0,len);
	        }
	        out.close();
	        in.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}

	public static void disablePlugin(String reason) {
		SIG.getPlugin().api = null;
		SIG.getPlugin().onDisable();
		Bukkit.getLogger().info("[SimpleInventoryManager] Plugin disabled : "+reason);
		Bukkit.getLogger().info("[SimpleInventoryManager] Please change your configuration and type 'sig reload'.");
	}
	
	public IDatabase openDatabaseConnection(String databaseClass) throws DatabaseConnectionException {

		@SuppressWarnings("rawtypes")
		Constructor constructor = null;
		try {
			constructor = ReflectionUtils.getConstructor(databaseClass+"DatabaseImpl", PackageType.SIG_PERSISTENCE);
		} catch (NoSuchMethodException | ClassNotFoundException e) {
			throw new DatabaseConnectionException("[SimpleInventoryGUI] "+ConfigurationParser.databaseClass+" is not a database valid value. It must be 'YAML', 'MySQL' or 'SQLite'.");
		}
		
		try {
			database = (IDatabase) constructor.newInstance();
			Bukkit.getLogger().info("[SimpleInventoryGUI] Using '"+databaseClass+"' for data persistence");
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new DatabaseConnectionException("[SimpleInventoryGUI] Cannot load the "+databaseClass+" database");
		}
		
		database.connect();
		
		return database;
	}

	public void closeDatabaseConnection() {
		if(database != null){
			Bukkit.getLogger().info("[SimpleInventoryGUI] Closing data persistence '"+ConfigurationParser.databaseClass+"'.");
			database.disconnect();
			database = null;
		}
	}
}
