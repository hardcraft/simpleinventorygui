package com.gmail.val59000mc.simpleinventorygui.items;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.configuration.Lang;
import com.gmail.val59000mc.spigotutils.Time;

public class GroupCooldown {
	private String name;
	private int cooldown;
	private Map<UUID,Long> playersNextUses;
	
	public GroupCooldown(String name, int cooldown){
		this.name = name;
		this.cooldown = cooldown;
		this.playersNextUses = Collections.synchronizedMap(new HashMap<UUID,Long>());
	}

	public String getName() {
		return name;
	}
	
	public boolean checkCooldown(Player player){
		UUID uuid = player.getUniqueId();
		Long nextUse = playersNextUses.get(uuid);
		Long now = Calendar.getInstance().getTimeInMillis();
		if(nextUse != null && nextUse > now){
			Long remainingTime = (nextUse-now)/1000;
			player.sendMessage(Lang.COOLDOWN_WAIT.replace("%time%", Time.getFormattedTime(remainingTime)));
			return false;
		}
		return true;
	}
	
	public void updateCooldown(UUID uuid){
		Long nextUse = playersNextUses.get(uuid);
		Long now = Calendar.getInstance().getTimeInMillis();
		if(nextUse == null || nextUse <= now){
			playersNextUses.put(uuid, now+(cooldown*1000));
		}
	}
}
