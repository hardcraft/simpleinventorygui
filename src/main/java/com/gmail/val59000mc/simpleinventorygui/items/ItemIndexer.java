package com.gmail.val59000mc.simpleinventorygui.items;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemIndexer{

	protected static Integer ID = 0;
	private static Map<String,Item> itemsIndex;
	private static Set<UUID> playersLocks;
	private static Map<String,GroupCooldown> groupCooldowns;
	
	public static void reset(){
		itemsIndex = Collections.synchronizedMap(new HashMap<String,Item>());
		groupCooldowns = Collections.synchronizedMap(new HashMap<String,GroupCooldown>());
		ID = 0;
		playersLocks = Collections.newSetFromMap(new ConcurrentHashMap<UUID, Boolean>());
	}
	
	
	
	
	// Group cooldowns
	
	public static boolean checkGroupCooldown(String name, Player player){
		if(groupCooldowns.containsKey(name)){
			return groupCooldowns.get(name).checkCooldown(player);
		}else{
			return true;
		}
	}
	
	public static void updateGroupCooldown(String name, UUID uuid){
		if(groupCooldowns.containsKey(name)){
			groupCooldowns.get(name).updateCooldown(uuid);
		}
	}
	
	public static void registerGroupCooldown(GroupCooldown groupCooldown){
		groupCooldowns.put(groupCooldown.getName(), groupCooldown);
	}
	
	
	
	
	
	// locks 
	
	public static boolean isLocked(UUID uuid){
		return  getPlayerLocks().contains(uuid);
	}
	
	public static void lock(UUID uuid){
		getPlayerLocks().add(uuid);
	}
	
	public static void unlock(UUID uuid){
		getPlayerLocks().remove(uuid);
	}
	
	private static synchronized Set<UUID> getPlayerLocks(){
		return playersLocks;
	}
	
	
	
	
	
	// items
	
	/**
	 * Get an item from its unique key
	 */
	private static Item findItem(String key){
		return itemsIndex.get(key);
	}
	
	/**
	 * Register an item in the index
	 */
	public static void indexItem(Item item){
		itemsIndex.put(item.getUniqueKey(), item);
	}
	
	public static void removeItemFromIndex(Item item){
		if(itemsIndex.containsKey(item.getUniqueKey())){
			itemsIndex.remove(item.getUniqueKey());
		}
	}

	public static Item findItem(ItemStack item) {
		if(item == null)
			return null;
		
		ItemMeta im = item.getItemMeta();
		if(im == null)
			return null;
		
		List<String> lore = im.getLore();
		if(lore == null)
			return null;
		
		if(lore != null && lore.size() > 0)
			return findItem(lore.get(lore.size() - 1));
		
		return null;
	}
}
