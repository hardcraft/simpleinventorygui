package com.gmail.val59000mc.simpleinventorygui.items;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;

import com.gmail.val59000mc.simpleinventorygui.actions.parsers.ActionParserManager;
import com.gmail.val59000mc.simpleinventorygui.configuration.ConfigurationParser;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ItemParseException;
import com.gmail.val59000mc.simpleinventorygui.items.Item.ItemBuilder;
import com.gmail.val59000mc.simpleinventorygui.util.ColorUtils;

public class ItemParser {

	public static ItemBuilder customizeItem(ItemBuilder builder, String customStr) throws ItemParseException{
		
		if(!customStr.isEmpty()){
			String[] customValue = customStr.split("=");
			if(customValue.length != 2){
				throw new ItemParseException("#Couldn't parse custom parameter +'"+customStr+"'. Syntax is param=value");
			}else{
				
				switch(customValue[0]){
					case "player-skull-name":
						builder.withPlayerSkullName(customValue[1]);
						break;
					case "color":
						
						Color color;
						try {
							color = ColorUtils.fromHexa(customValue[1]);
						} catch (Exception e1) {
							throw new ItemParseException("#Couldn't parse custom parameter +'"+customStr+"'. Syntax is color=#FFFFFF"); 
						}
						builder.withColor(color);

						break;
					case "unbreakable":
						builder.withUnbreakable(Boolean.parseBoolean(customValue[1]));
						break;
					case "player-skull-texture":
						builder.withPlayerSkullTexture(customValue[1]);
						break;
					case "page":
						String page = "[{text:\"" + customValue[1] + "\"}]";
						builder.addBookPage(page);
						break;
					case "pagejson":
						builder.addBookPage(customValue[1]);
						break;
					case "hideflags":
						builder.withHideFlags(Boolean.parseBoolean(customValue[1]));
						break;
					case "flagcolor":
						
						DyeColor dyeColor;
						try {
							dyeColor = DyeColor.valueOf(customValue[1]);
						} catch (Exception e1) {
							throw new ItemParseException("#Couldn't parse custom parameter +'"+customStr+"'. Syntax is flagcolor=a_dye_color"); 
						}
						builder.withFlagColor(dyeColor);

						break;
					default:
						throw new ItemParseException("#Unkonwn item customization attribute '"+customStr+"'");
				}
				
			}
		}
		
		return builder;
	}
	
	public static Item parseItemSection(ConfigurationSection itemSection) throws ItemParseException {

		ItemBuilder builder;
		
		
		/////////////////
		// Item string //
		String itemStr = itemSection.getString("item");
		if(itemStr == null){
			throw new ItemParseException("#Item "+itemSection.getName()+" must provide an 'item' attribute");
		}

		builder = parseItemString(itemStr);
		
		
		
		
		/////////////////////
		// Position string //
		String posStr = itemSection.getString("position");
		if(posStr == null){
			posStr = "1:1";
		}
		/*
		 * posStr[0] = line
		 * posStr[1] = case
		 */
		String[] posArr = posStr.split(":");
		if(posArr.length != 2)
			throw new ItemParseException("#Item position must be formatted like 'line:position_in_the_line', example '2;9'");
		int line;
		int posInLine;
		try {
			line  = Integer.parseInt(posArr[0]);
			posInLine = Integer.parseInt(posArr[1]);
		} catch (NumberFormatException e) {
			throw new ItemParseException("#Item position arguments must be numbers, example '1:5'");
		}
		
		if(line < 1 || line > 6){
			throw new ItemParseException("#The line number ranges from 1 to 6");
		}
		
		if(posInLine < 1 || posInLine > 9){
			throw new ItemParseException("#The position in line ranges from 1 to 9");
		}
		
		int position = 9*(line-1)+(posInLine-1);
		
		builder.withPosition(position);
		
		
		/////////////////////
		// Cooldown String //
		String cooldownName = itemSection.getString("cooldown");
		Integer cooldown = ConfigurationParser.defaultItemCooldown;
		builder.withCooldown(cooldown);
		if(cooldownName != null){
			try{
				cooldown = Integer.parseInt(cooldownName);
				if(cooldown < 0 || cooldown > 10000)
					throw new ItemParseException("#Item cooldown must be between 0 and 10000");
				builder.withCooldown(cooldown);
			}catch(NumberFormatException e){
				builder.withGroupCooldown(cooldownName);
			}
		}
		
		
		/////////////////
		// Permissions //
		List<String> permissions = itemSection.getStringList("permissions");
		if(permissions == null){
			permissions = new ArrayList<String>();
		}
		
		builder.withPermissions(permissions);

		
		
		
		////////////////////
		// Action section //
		ConfigurationSection actionSection = itemSection.getConfigurationSection("actions");
		
		
		

		////////////////////////
		// item-customization //
		////////////////////////
		List<String> itemCustomizationStrList = itemSection.getStringList("item-customization");
		if(itemCustomizationStrList != null){
			for(String customizeStr : itemCustomizationStrList){
				customizeItem(builder,customizeStr);
			}
		}
		
		
		Item item = builder.build();
		
		try {
			item.setActions(ActionParserManager.instance().parseAllActionsSection(actionSection));
		} catch (ActionParseException e) {
			throw new ItemParseException("#When parsing actions : "+e.getMessage());
		}
		
		return item;
	}
	
	
	public static ItemBuilder parseItemString(String itemStr) throws ItemParseException{
		/*
		 * itemArr[0] = material
		 * itemArr[1] = damage
		 * itemArr[2] = amount
		 * itemArr[3] = name
		 * itemArr[4] = lore,lore,lore
		 * itemArr[5] = enchant=level,enchant=level
		 */
		String[] itemArr = itemStr.split(":");
		if(itemArr.length==0){
			throw new ItemParseException("#The item string cannot be empty. You should at least provide a material name, example 'iron_sword'");
		}
		
		Material material;
		try {
			material = Material.valueOf(itemArr[0].toUpperCase());
		} catch (IllegalArgumentException e) {
			throw new ItemParseException("#The material name "+itemArr[0]+" is unknown, look at the available material list here : https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/Material.html");
		}
		
		Item.ItemBuilder builder = new Item.ItemBuilder(material);
		
		if(itemArr.length >= 2){
			short damage;
			try {
				damage = Short.parseShort(itemArr[1]);
			} catch (NumberFormatException e) {
				throw new ItemParseException("#The damage value must be a number");
			}
			builder.withDamage(damage);
		}
		
		if(itemArr.length >= 3){
			int amount;
			try {
				amount = Short.parseShort(itemArr[2]);
			} catch (NumberFormatException e) {
				throw new ItemParseException("#The amount value must be a number");
			}
			builder.withAmount(amount);
		}
		
		if(itemArr.length >= 4){
			builder.withName(ChatColor.translateAlternateColorCodes('&',itemArr[3]));
		}
		
		if(itemArr.length >= 5){
			String[] loreArr = itemArr[4].split(",");
			List<String> lore = new ArrayList<String>();
			for(String loreLine : loreArr){
				lore.add(ChatColor.translateAlternateColorCodes('&',loreLine));
			}
			builder.withLore(lore);
		}
		
		if(itemArr.length >= 6){
			Map<Enchantment,Integer> enchantments = new HashMap<Enchantment,Integer>();
			String[] allEnchArr = itemArr[5].split(",");
			for(String enchStr : allEnchArr){
				String[] enchArr = enchStr.split("=");
				if(enchArr.length != 2){
					throw new ItemParseException("#Enchantments must be formated like ENCH=LEVEL,ENCH=LEVEL example 'DAMAGE_ALL=1,KNOCKBACK=2'");
				}else{
					Enchantment enchantment;
					try {
						enchantment = Enchantment.getByName(enchArr[0].toUpperCase());
					} catch (IllegalArgumentException e) {
						throw new ItemParseException("#Enchantments must be in that list : https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/enchantments/Enchantment.html");
					}
					int level;
					try {
						level = Integer.parseInt(enchArr[1]);
						if(level < 1 || level > 1000){
							throw new NumberFormatException();
						}
					} catch (NumberFormatException e) {
						throw new ItemParseException("#The ennchantment level must be betwenn 1 and 1000");
					}
					
					enchantments.put(enchantment, level);
				}
			}
			builder.withEnchantments(enchantments);
		}
		
		return builder;
	}

}
