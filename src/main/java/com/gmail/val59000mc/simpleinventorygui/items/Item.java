package com.gmail.val59000mc.simpleinventorygui.items;

import com.gmail.val59000mc.simpleinventorygui.ActionExecutor;
import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.configuration.ConfigurationParser;
import com.gmail.val59000mc.simpleinventorygui.configuration.Lang;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryManager;
import com.gmail.val59000mc.simpleinventorygui.placeholders.PlaceholderManager;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.spigotutils.Time;
import com.gmail.val59000mc.spigotutils.items.PotionMetaBuilder;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.*;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionType;

import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Logger;

public class Item implements ActionExecutor {

    private static MessageDigest md5;

    private Material material;
    private short damage;
    private int amount;
    private String name;
    private List<String> lore;
    private List<PotionEffect> potionEffects;
    private List<String> pages;
    private Map<Enchantment, Integer> enchantments;
    private List<Action> actions;
    private int position;
    private String uniqueKey;
    private List<String> permissions;
    private ItemModifier itemModifier;
    private int cooldown;
    private String groupCooldown;
    private Map<UUID, Long> playersNextUses;

    // Custmization
    private String playerSkullName;
    private String playerSkullTexture;
    private Color color;
    private Boolean unbreakable;
    private boolean hideFlags;
    private DyeColor dyeColor;

    // Private constructor
    private Item(ItemBuilder builder) {
        this.material = builder.material;
        this.damage = builder.damage;
        this.amount = builder.amount;
        this.name = builder.name;
        this.lore = builder.lore;
        this.potionEffects = builder.potionEffects;
        this.pages = builder.pages;
        this.enchantments = builder.enchantments;
        this.position = builder.position;
        this.actions = builder.actions;
        this.permissions = builder.permissions;
        this.cooldown = builder.cooldown;
        this.groupCooldown = builder.groupCooldown;
        this.itemModifier = null;
        this.uniqueKey = createUniqueKey(builder.requireUniqueKey);

        this.playersNextUses = Collections.synchronizedMap(new HashMap<UUID, Long>());

        // Customization
        this.playerSkullName = builder.playerSkullName;
        this.playerSkullTexture = builder.playerSkullTexture;
        this.color = builder.color;
        this.unbreakable = builder.unbreakable;
        this.hideFlags = builder.hideFlags;
        this.dyeColor = builder.dyeColor;
    }

    /**
     * Generates a unique md5 hash based on toStrigable parameters
     *
     * @return
     */
    private String createUniqueKey(boolean requireUniqueKey) {
        String key = "";

        try {
            if (md5 == null) {
                md5 = MessageDigest.getInstance("MD5");
            }

            // Generating the string to hash
            String bigString = "";
            bigString += this.name;
            bigString += this.material.toString();
            bigString += this.amount;
            bigString += this.position;
            bigString += this.damage;
            for (String loreLine : this.lore) {
                bigString += loreLine;
            }

            if (requireUniqueKey) {
                bigString += UUID.randomUUID().toString();
            }

            bigString += ItemIndexer.ID;
            ItemIndexer.ID++;

            md5.update(bigString.getBytes());
            byte[] digest = md5.digest();

            StringBuffer sb = new StringBuffer();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }

            key = sb.toString();

        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

        // Insert special char between each char to make the string invisible in a lore
        StringBuilder hiddenKey = new StringBuilder();
		hiddenKey.append(ChatColor.COLOR_CHAR + "0");
        for (char c : key.toCharArray()) {
            hiddenKey.append(ChatColor.COLOR_CHAR);
            hiddenKey.append(c);
        }

        return hiddenKey.toString();

    }

    private void replaceItemMetaPlaceholders(ItemStack stack, Player player, SigPlayer sigPlayer) {
        ItemMeta im = stack.getItemMeta();

        PlaceholderManager pm = PlaceholderManager.instance();

        if (im.getDisplayName() != null) {
            im.setDisplayName(pm.replacePlaceholders(im.getDisplayName(), player, sigPlayer));
        }

        if (im.getLore() != null) {
            List<String> lore = new ArrayList<String>();
            for (String loreLine : im.getLore()) {
                String replacement = pm.replacePlaceholders(loreLine, player, sigPlayer);
                String[] replacementArr = replacement.split("<br>");
                lore.addAll(Lists.newArrayList(replacementArr));
            }
            im.setLore(lore);
        }

        if (im instanceof SkullMeta) {
            if (this.playerSkullName != null) {
                ((SkullMeta) im).setOwner(pm.replacePlaceholders(this.playerSkullName, player, sigPlayer));
            }
            if (this.playerSkullTexture != null) {


                GameProfile profile = new GameProfile(UUID.randomUUID(), null);
                byte[] encodedData = Base64.getEncoder().encode(String.format("{textures:{SKIN:{url:\"%s\"}}}", this.playerSkullTexture).getBytes());
                profile.getProperties().put("textures", new Property("textures", new String(encodedData)));
                Field profileField = null;
                try {
                    profileField = im.getClass().getDeclaredField("profile");
                    profileField.setAccessible(true);
                    profileField.set(im, profile);
                } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
                    e1.printStackTrace();
                }

            }
        }

        if (im instanceof LeatherArmorMeta && this.color != null) {
            ((LeatherArmorMeta) im).setColor(this.color);
        }

        if (this.unbreakable != null) {
            im.spigot().setUnbreakable(this.unbreakable);
        }

        if (this.hideFlags) {
            im.addItemFlags(
                ItemFlag.HIDE_ATTRIBUTES,
                ItemFlag.HIDE_DESTROYS,
                ItemFlag.HIDE_ENCHANTS,
                ItemFlag.HIDE_PLACED_ON,
                ItemFlag.HIDE_POTION_EFFECTS,
                ItemFlag.HIDE_UNBREAKABLE
            );
        }

        if (this.dyeColor != null && im instanceof BannerMeta) {
            ((BannerMeta) im).setBaseColor(dyeColor);
        }

        if (im instanceof BookMeta) {
            if (!this.pages.isEmpty()) {

                ((BookMeta) im).setPages(new ArrayList<String>());
                BookMetaGenerator generator = new BookMetaGenerator(((BookMeta) im));

                for (String page : this.pages) {
                    generator.addPageInJson(pm.replacePlaceholders(page, player, sigPlayer));
                }

                im = generator.build();

            }
        }

        stack.setItemMeta(im);
    }

    /*
     * Register the item actions
     * Also check if an action is an ItemModifier and if so, register it
     * Only register the firt modifier if found
     */
    public Item setActions(List<Action> actions) {
        this.actions = actions;
        for (Action action : this.actions) {
            action.setActionExecutor(this);
            if (action instanceof ItemModifier) {
                ItemModifier modifier = (ItemModifier) action;
                if (modifier.isModifier() && modifier != null) {
                    registerItemModifier(modifier);
                }
            }
        }
        return this;
    }

    private void registerItemModifier(ItemModifier itemModifier) {
        this.itemModifier = itemModifier;
    }

    /**
     * Gets the item that will be used to build the item stack
     * Can be the item itself or a registered item modifier if not null
     *
     * @param player
     * @return
     */
    private Item getSourceItem(Player player, SigPlayer sigPlayer) {

        Item source = this;

        if (itemModifier != null) {
            source = itemModifier.getAlternateVersion(player, sigPlayer);
        }

        return source;
    }

    /**
     * Build item with unique key and modifiers
     *
     * @param player
     * @return
     */
    public ItemStack buildItem(Player player, SigPlayer sigPlayer) {

        Item source = getSourceItem(player, sigPlayer);

        ItemStack stack = new ItemStack(source.material, source.amount, source.damage);
        ItemMeta im = stack.getItemMeta();
        if (!" ".equals(source.name)) {
            im.setDisplayName(source.name);
        }

        List<String> newLore = new ArrayList<>(source.lore);
        newLore.add(this.uniqueKey);
        im.setLore(newLore);

        if (im instanceof EnchantmentStorageMeta) {
            EnchantmentStorageMeta enchMeta = (EnchantmentStorageMeta) im;
            for (Entry<Enchantment, Integer> entry : enchantments.entrySet()) {
                enchMeta.addStoredEnchant(entry.getKey(), entry.getValue(), true);
            }
        } else {
            for (Entry<Enchantment, Integer> entry : enchantments.entrySet()) {
                im.addEnchant(entry.getKey(), entry.getValue(), true);
            }
        }

        if(im instanceof PotionMeta) {
            PotionMeta potionMeta = (PotionMeta) im;
            PotionEffect firstEffect = Iterables.getFirst(potionEffects, null);
            if(firstEffect != null) {
                potionMeta.setBasePotionData(new PotionData(PotionType.getByEffect(firstEffect.getType()), false, false));
            }
            for(PotionEffect potionEffect : potionEffects) {
                potionMeta.addCustomEffect(potionEffect, true);
            }
        }

        stack.setItemMeta(im);

        replaceItemMetaPlaceholders(stack, player, sigPlayer);
        source.replaceItemMetaPlaceholders(stack, player, sigPlayer);

        return stack;
    }

    /**
     * Build item without unique key and without modifiers
     *
     * @param player
     * @return
     */
    public ItemStack buildRawItem(Player player, SigPlayer sigPlayer) {
        ItemStack stack = new ItemStack(material, amount, damage);
        ItemMeta im = stack.getItemMeta();
        if (!" ".equals(this.name)) {
            im.setDisplayName(this.name);
        }

        if (this.lore.size() > 0 && !" ".equals(this.lore.get(0))) {
            List<String> newLore = new ArrayList<String>(this.lore);
            im.setLore(newLore);
        }

        if (im instanceof EnchantmentStorageMeta) {
            EnchantmentStorageMeta enchMeta = (EnchantmentStorageMeta) im;
            for (Entry<Enchantment, Integer> entry : enchantments.entrySet()) {
                enchMeta.addStoredEnchant(entry.getKey(), entry.getValue(), true);
            }
        } else {
            for (Entry<Enchantment, Integer> entry : enchantments.entrySet()) {
                im.addEnchant(entry.getKey(), entry.getValue(), true);
            }
        }

        if(im instanceof PotionMeta) {
            PotionMeta potionMeta = (PotionMeta) im;
            PotionEffect firstEffect = Iterables.getFirst(potionEffects, null);
            if(firstEffect != null) {
                potionMeta.setBasePotionData(new PotionData(PotionType.getByEffect(firstEffect.getType()), false, false));
            }
            for(PotionEffect potionEffect : potionEffects) {
                potionMeta.addCustomEffect(potionEffect, true);
            }
        }

        stack.setItemMeta(im);

        replaceItemMetaPlaceholders(stack, player, sigPlayer);

        return stack;
    }

    public boolean checkPermission(Player player) {
        for (String node : permissions) {
            if (!player.hasPermission(node)) {
                player.sendMessage(Lang.NO_ITEM_PERMISSION);
                return false;
            }
        }
        return true;
    }

    public boolean checkCooldown(Player player) {
        if (groupCooldown != null) {
            return ItemIndexer.checkGroupCooldown(groupCooldown, player);
        } else if (cooldown > 0) {
            UUID uuid = player.getUniqueId();
            Long nextUse = playersNextUses.get(uuid);
            Long now = Calendar.getInstance().getTimeInMillis();
            if (nextUse != null && nextUse > now) {
                Long remainingTime = (nextUse - now) / 1000;
                player.sendMessage(Lang.COOLDOWN_WAIT.replace("%time%", Time.getFormattedTime(remainingTime)));
                return false;
            }
        }
        return true;
    }

    public void updateCooldown(UUID uuid) {
        if (groupCooldown != null) {
            ItemIndexer.updateGroupCooldown(groupCooldown, uuid);
        } else if (cooldown > 0) {
            Long nextUse = playersNextUses.get(uuid);
            Long now = Calendar.getInstance().getTimeInMillis();
            if (nextUse == null || nextUse <= now) {
                playersNextUses.put(uuid, now + (cooldown * 1000));
            }
        }
    }

    public void print() {
        Logger l = Bukkit.getLogger();
        l.info("-----------------------------");
        l.info("name: " + name);
        l.info("material: " + material.toString());
        l.info("amount: " + amount);
        l.info("damage: " + damage);
        l.info("unique_key: " + uniqueKey);
        l.info("-----------------------------");
    }

    public int getPosition() {
        return position;
    }

    public String getUniqueKey() {
        return this.uniqueKey;
    }

    /**
     * Item builder helper
     * Only way to instantiate an Item, which constructor is private
     *
     * @author Valentin
     */
    public static class ItemBuilder {
        private Material material;
        private short damage;
        private int amount;
        private String name;
        private List<String> lore;
        private List<PotionEffect> potionEffects;
        private List<String> pages;
        private Map<Enchantment, Integer> enchantments;
        public List<Action> actions;
        public int position;
        private List<String> permissions;
        private int cooldown;
        private String groupCooldown;
        private boolean requireUniqueKey;

        // Customization
        private String playerSkullName;
        private String playerSkullTexture;
        private Color color;
        private Boolean unbreakable;
        private boolean hideFlags;
        private DyeColor dyeColor;

        public ItemBuilder(Material material) {
            this.material = material;
            this.damage = 0;
            this.amount = 1;
            this.name = " ";
            this.lore = new ArrayList<>();
            this.potionEffects = new ArrayList<>();
            this.pages = new ArrayList<>();
            this.enchantments = new HashMap<>();
            this.actions = new ArrayList<>();
            this.position = 0;
            this.permissions = new ArrayList<>();
            this.cooldown = ConfigurationParser.defaultItemCooldown;
            this.groupCooldown = null;
            this.requireUniqueKey = false;

            // Customization
            this.playerSkullName = null;
            this.playerSkullTexture = null;
            this.color = null;
            this.unbreakable = null;
            this.hideFlags = false;
        }

        public ItemBuilder withRequireUniqueKey(boolean requireUniqueKey) {
            this.requireUniqueKey = requireUniqueKey;
            return this;
        }

        public ItemBuilder withDamage(short damage) {
            this.damage = damage;
            return this;
        }

        public ItemBuilder withPlayerSkullName(String name) {
            this.playerSkullName = name;
            return this;
        }

        public ItemBuilder withPlayerSkullTexture(String playerSkullTexture) {
            this.playerSkullTexture = playerSkullTexture;
            return this;
        }

        public ItemBuilder withPosition(int position) {
            this.position = position;
            return this;
        }

        public ItemBuilder withAmount(int amount) {
            this.amount = amount;
            return this;
        }

        public ItemBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public ItemBuilder withLore(List<String> lore) {
            this.lore = lore;
            return this;
        }

        public ItemBuilder addPotionEffect(PotionEffect potionEffect) {
            this.potionEffects.add(potionEffect);
            return this;
        }

        public ItemBuilder addBookPage(String page) {
            this.pages.add(page);
            return this;
        }

        public ItemBuilder addBookPages(List<String> pages) {
            this.pages.addAll(pages);
            return this;
        }

        public ItemBuilder withEnchantments(Map<Enchantment, Integer> enchantments) {
            this.enchantments = enchantments;
            return this;
        }

        public ItemBuilder withPermissions(List<String> permissions) {
            this.permissions = permissions;
            return this;
        }

        public ItemBuilder withCooldown(Integer cooldown) {
            this.cooldown = cooldown;
            return this;
        }

        public ItemBuilder withGroupCooldown(String groupCooldown) {
            this.groupCooldown = groupCooldown;
            return this;
        }

        public ItemBuilder withColor(Color color) {
            this.color = color;
            return this;
        }

        public ItemBuilder withUnbreakable(Boolean unbreakable) {
            this.unbreakable = unbreakable;
            return this;
        }

        public ItemBuilder withHideFlags(boolean hideFLags) {
            this.hideFlags = hideFLags;
            return this;
        }

        public ItemBuilder withFlagColor(DyeColor dyeColor) {
            this.dyeColor = dyeColor;
            return this;
        }

        public Item build() {
            return new Item(this);
        }

    }


    public void executeAllActionsAsynchronously(UUID playerUuid, SigPlayer sigPlayer) {

        if (!ItemIndexer.isLocked(playerUuid) && !getActions().isEmpty()) {
            Player player = Bukkit.getPlayer(playerUuid);
            if (player != null && player.isOnline()) {
                if (checkPermission(player)) {
                    if (checkCooldown(player)) {
                        ItemIndexer.lock(playerUuid);
                        getActions().get(0).executeAction(player, sigPlayer);
                    }
                }

            }
        }

    }

    public synchronized List<Action> getActions() {
        return actions;
    }

    @Override
    public void endActionsExecution(Player player, SigPlayer sigPlayer, boolean success) {
        UUID uuid = player.getUniqueId();
        ItemIndexer.unlock(uuid);
        if (success) {
            updateCooldown(uuid);
        }
        if (player.isOnline()) {
            InventoryManager.instance().updateInventoryView(player, sigPlayer);

        }

    }

}
