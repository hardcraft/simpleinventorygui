package com.gmail.val59000mc.simpleinventorygui.items;

import net.minecraft.server.v1_14_R1.IChatBaseComponent;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftMetaBook;
import org.bukkit.inventory.meta.BookMeta;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Generate a different BookMeta with JSON!
 *
 * @author 67clement
 */
public class BookMetaGenerator {

    private BookMeta itemMeta;
    private List<String> pages;

    public BookMetaGenerator(BookMeta itemMeta) {
        this.itemMeta = itemMeta;
        this.pages = new ArrayList<String>();
        for (String page : itemMeta.getPages()) {
            this.addPage(page);
        }
    }

    public void addPage(String page) {
        this.pages.add("[{text:\"" + page + "\"}]");
    }

    public void addPageInJson(String json) {
        this.pages.add(json);
    }

    public void setPage(int index, String page) {
        this.pages.set(index, "[{text:\"" + page + "\"}]");
    }

    public void setPageInJson(int index, String json) {
        this.pages.set(index, json);
    }

    public void setPages(List<String> pages) {
        pages.clear();
        for (String page : pages) {
            this.addPage(page);
        }
    }

    public void setPagesInJson(List<String> pages) {
        pages.clear();
        for (String page : pages) {
            this.addPageInJson(page);
        }
    }

    public String getPage(int index) {
        return this.pages.get(index);
    }

    public List<String> getPages() {
        return this.pages;
    }

    public BookMeta build() {
        List<IChatBaseComponent> nms_Pages = new ArrayList<IChatBaseComponent>();
        for (String page : this.getPages()) {
            nms_Pages.add(IChatBaseComponent.ChatSerializer.b(page));
        }

        CraftMetaBook book = ((CraftMetaBook) this.itemMeta);

        try {
            Field field = book.getClass().getField("pages");

            field.setAccessible(true);
            field.set(book, nms_Pages);
        } catch (IllegalArgumentException | IllegalAccessException
            | NoSuchFieldException | SecurityException e) {
            e.printStackTrace();
        }

        return book;

    }
}
