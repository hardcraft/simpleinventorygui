package com.gmail.val59000mc.simpleinventorygui.items;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public interface ItemModifier {
	/*
	 * Get an alternate version of an Item 
	 */
	public Item getAlternateVersion(Player player, SigPlayer sigPlayer);
	
	/**
	 * A class can implements the ItemModifier interface without actually needing to modify an item
	 * So this method check if we should get the alternate version of the item or not
	 * @return true if a alternate version is available, false if not
	 */
	public boolean isModifier();
}
